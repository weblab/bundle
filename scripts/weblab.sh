#!/bin/sh
#
# WebLab manager Script
#

DIRNAME=`pwd`
CURRENT_USER=`id -u`

if [ $CURRENT_USER = "0" ]
then 
    echo "ERROR: Must not be launch as root" >&2
    exit 1
fi

# Set the path to the java executable
JAVA="$JAVA_HOME/bin/java"
[ -x "$JAVA" ] || JAVA="`which java`" || {
        echo "ERROR: java executable not found" >&2
        exit 1
}

# Set the path to weblab bootstrap jar
WEBLAB_JARS="$DIRNAME/weblab-launcher.jar:$DIRNAME/data/lib/*"

exec $JAVA -cp "$WEBLAB_JARS" -Dlog4j.configuration="file:$DIRNAME/data/lib/log4j.properties" org.ow2.weblab.bundle.Launcher "$@"
