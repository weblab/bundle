@echo off
::
::   WebLab manager Script
::

:: Set the path to the java executable
if "x%JAVA_HOME%"=="x" (
   set JAVA=java
) else (
   set JAVA=%JAVA_HOME%\bin\java.exe
)

set WEBLAB_LAUNCHER="%CD%\weblab-launcher.jar"
if not exist %WEBLAB_LAUNCHER% (
   echo "ERROR: weblab-launcher.jar: jar archive not found"
   goto :EOF
)

set WEBLAB_JARS="%WEBLAB_LAUNCHER%;data\lib\*"

call "%JAVA%" -cp %WEBLAB_JARS% -Dlog4j.configuration=file:%CD%\data\lib\log4j.properties org.ow2.weblab.bundle.Launcher %*

pause