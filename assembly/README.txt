Welcome to WebLab Bundle ${project.version}
**************************************************************************************

WebLab Bundle aims to gather coherent services and portlets around the WebLab platform to
demonstrate its capability in term of service integration and orchestration for
unstructured document processing and retrieval.
This bundle listen to a local folder (toIndex) in order to analyse office based documents (doc, ppt, pdf ...),
index them to finally offer access to them through a portal.
The processing capabilities are limited (only default rules for the named-entity extraction engine are used)
but it allows to have a complete processing chain and ease integration and tests of
new components either on processing chain or on user interface.
This bundle is regularly released (http://weblab-project.org/index.php?title=Download)
and build nightly with latest services/portlets (see http://bamboo.ow2.org/browse/WEBLAB-BUNDLE).
For an up-to-date description, please refers to http://weblab-project.org/index.php?title=Bundle

This bundle presents an information retrieval system based on the complete WebLab architecture.
It is mainly composed of the following WebLab servers:
	* apache-tomcat publishing web services
	* liferay publishing portlets
	* solr-clould and zookeeper enabling distributed indexing/search of documents
	* activemq providing JMS
	* customised karaf framework providing camel, web application and weblab features

WebLab Bundle provide following tools open sources libraries as web services:
	* Gate: named entities extractor
	* Ngramj: a language detector
	* SolR indexer: a full text indexer/searcher
	* Tika: a metadata extractor and normaliser for office documents

and WebLab portlets:
	* a search portlet that will launch query with filters on the SolR searcher,
	* a facet portlet that allows users to refine their query with extracted entities/metadata
	* a result portlet that displays the results of the query,
	* a annotated document portlet that displays the document annotated with the annotation added by the named entities extraction service.
	* a metadata portlet that displays metadata found in processed document

Processing chains:
	1. files processing: it listens to the 'data/toIndex' directory for office documents to process with previous services
	1. warcs processing: it listens to the 'data/warcs' directory for Warcs (Web archive) files to process with previous services

Getting Started
**************************************************************************************
You should have a jdk 1.8 or greater installed in order to run the WebLab bundle,
JAVA_HOME must be declared and java must be available in your path
Ports 8080, 8005, 8009, 18080 (Liferay), 8181, 8105, 8109, 18181 (Tomcat), 1099, 8282, 8383 (Karaf), 1616, 8161, 61616 (ActiveMQ), 2181 (Zookeeper), 8983 (Solr) must be available.
Your computer should have at least a 4 core processor and 6Go of RAM to run the WebLab bundle.
Remember that WebLab is a server application and not a desktop one.

Running
**************************************************************************************
   1. Launch the WebLab with "weblab.sh start" (Linux) or "weblab.bat start" (Windows) regarding your OS, (It may takes several minutes)
   2. Go with your favourite browser to http://localhost:8080/

File processing chain is started automatically, go to the 'Search' tab, then you can start a search for processed documents.

More details
**************************************************************************************
More details and information about the Bundle are available at the following address: http://weblab-project.org/index.php?title=Bundle
