/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.bundle.utils;

/**
 * Execute a method and return an Object
 *
 * @author asaval
 * @param <T>
 *            The returned type of the status after execution
 */
public abstract class Executor<T> {


	private final boolean displayingFail;


	T status;


	Exception lastException;


	public Executor(final boolean displayingFail) {
		this.displayingFail = displayingFail;
	}


	public boolean isDisplayingFail() {
		return this.displayingFail;
	}


	public T getStatus() {
		return this.status;
	}


	public Exception getLastException() {
		return this.lastException;
	}


	/**
	 * Execute a method and return a &lt;T&gt; Object or throws an Exception
	 *
	 * @return an Object of type &lt;T&gt;
	 * @throws Exception
	 *             if something went wrong.
	 */
	public abstract T execute() throws Exception;


	public void start() {
		final Thread thread = new Thread() {


			@Override
			public void run() {
				try {
					Executor.this.status = Executor.this.execute();
				} catch (final Exception exception) {
					Executor.this.lastException = exception;
				}
			}
		};
		thread.start();
	}
}
