/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space SAS
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.bundle.server;

import java.io.File;
import java.io.IOException;

import javax.management.MBeanServerConnection;
import javax.management.MBeanServerInvocationHandler;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;

import org.apache.activemq.broker.jmx.BrokerViewMBean;
import org.apache.activemq.broker.jmx.QueueViewMBean;
import org.ow2.weblab.bundle.utils.PathUtils;
import org.ow2.weblab.bundle.utils.ProcessUtils;
import org.ow2.weblab.bundle.utils.Utils;

/**
 * Active MQ server used by processing chain
 *
 * @author lmartin, ymombrun
 */
public class ActiveMQServer extends AbstractApplicationServer {


	private String processIdentificationClue;


	private String dataDir;


	/**
	 * The default constructor
	 */
	public ActiveMQServer() {

		this.setMandatory(true);
	}


	/**
	 * Ensure data folder exists (to prevent warning in activemq start script)
	 */
	@Override
	protected void checkConfiguration() {

		if (this.dataDir == null) {
			this.logger.debug("No dataDir property configured for activemq. There will be a few logs in activemq start script.");
		} else {
			final File theDataDir = new File(this.dataDir);
			if (!theDataDir.exists() && !theDataDir.mkdir()) {
				this.logger.warn("Unable to create data directory for activemq at " + theDataDir.getAbsolutePath());
			}
		}
	}



	@Override
	protected String[] getStartScript() {

		final String ext = System.getProperty("os.name").contains("Windows") ? ".bat" : "";
		return new String[] { PathUtils.createPath(this.getHome(), "bin") + "activemq" + ext, WebLabServer.START };
	}


	@Override
	protected String[] getStopScript() {

		final String ext = System.getProperty("os.name").contains("Windows") ? ".bat" : "";
		return new String[] { PathUtils.createPath(this.getHome(), "bin") + "activemq" + ext, WebLabServer.STOP };
	}


	@Override
	public JMXConnector getJMXConnector() {

		return ProcessUtils.createJMXclient(this.getHost(), this.getJmxPort(), "", "", "jmxrmi", 2000, false, this.logger);
	}



	@Override
	public String getProcessIdentificationClue() {

		return this.processIdentificationClue;
	}


	/**
	 * @param processIdentificationClue
	 *            the processIdentificationClue to set
	 */
	public void setProcessIdentificationClue(final String processIdentificationClue) {

		this.processIdentificationClue = processIdentificationClue;
	}


	@Override
	public String supportedCommands() {

		return this.getName() + " [start|stop|restart|status|[queue] purge] \t will start, stop and give information on " + this.getName() + " current status. Last command purges a given queue.\n";
	}


	@Override
	public boolean isServerFullyStarted(final boolean showDetails) {

		if (!this.isEnabled()) {
			this.logger.info("Can not check if " + this.getName() + " is started since control is disabled.");
			return true;
		}
		this.logger.info("Checking " + this.getName() + " status ... ");
		if (!Utils.isPortAvailable(this.logger, this.getHost(), this.getPort())) {
			this.logger.info(this.getName() + " is started.");
			if (showDetails) {
				this.listQueuesStatus();
			}
			return true;
		}
		this.logger.info(this.getName() + " is not started.");
		return false;
	}


	/**
	 * @param connection
	 *            An already opened MBean connection
	 * @return The list of queues
	 */
	public ObjectName[] listQueues(final MBeanServerConnection connection) {

		final ObjectName activeMQ;
		try {
			activeMQ = new ObjectName("org.apache.activemq:type=Broker,brokerName=" + this.getHost());
		} catch (final MalformedObjectNameException mone) {
			this.logger.warn("Invalid object name Broker!", mone);
			return null;
		}

		final BrokerViewMBean broker = MBeanServerInvocationHandler.newProxyInstance(connection, activeMQ, BrokerViewMBean.class, true);

		return broker.getQueues();
	}


	private void listQueuesStatus() {

		try (final JMXConnector connector = this.getJMXConnector()) {
			final MBeanServerConnection connection = connector.getMBeanServerConnection();
			if (connection == null) {
				this.logger.info("Unable to connect to " + this.getName() + " to retrieve list of queues.");
				return;
			}
			for (final ObjectName queueName : this.listQueues(connection)) {
				final QueueViewMBean queueMbean = MBeanServerInvocationHandler.newProxyInstance(connection, queueName, QueueViewMBean.class, true);
				this.logger.info("\tQueue: " + queueMbean.getName() + " has currently " + queueMbean.getQueueSize() + " enqueued message(s). Since it's creation, a total of "
						+ queueMbean.getEnqueueCount() + " message(s) have been enqueued.");
			}
		} catch (final IOException ioe) {
			this.logger.warn("Unable to list queues due to a connection issue.", ioe);
		}
	}


	/**
	 * @param queue
	 *            The name of the queue to purge. If null, then purge all queues
	 */
	public void purge(final String queue) {

		boolean queueFound = false;
		try (final JMXConnector connector = this.getJMXConnector()) {
			final MBeanServerConnection connection = connector.getMBeanServerConnection();
			if (connection == null) {
				this.logger.info("Unable to connect to " + this.getName() + " to retrieve list of queues.");
				return;
			}
			for (final ObjectName queueName : this.listQueues(connection)) {
				final QueueViewMBean queueMbean = MBeanServerInvocationHandler.newProxyInstance(connection, queueName, QueueViewMBean.class, true);
				if ((queue == null) || queue.equals(queueMbean.getName())) {
					queueFound = true;
					if (queueMbean.getQueueSize() > 0) {
						this.logger.info("Purging queue " + queueMbean.getName() + ".");
						queueMbean.purge();
					} else {
						this.logger.info("Queue " + queueMbean.getName() + " is empty. Nothing to purge.");
					}
				}
			}
		} catch (final Exception e) {
			queueFound = true; // To prevent from another log message after
			this.logger.warn("An error occurred purging queue " + queue + ".", e);
		}

		if (!queueFound) {
			this.logger.warn("Queue " + queue + " does not exist. Nothing to purge.");
		}
	}


	/**
	 * @return the dataDir
	 */
	public String getDataDir() {

		return this.dataDir;
	}


	/**
	 * @param dataDir
	 *            the dataDir to set
	 */
	public void setDataDir(final String dataDir) {

		this.dataDir = dataDir;
	}

}
