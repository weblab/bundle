/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space SAS
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.bundle.server;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.RuntimeMXBean;
import java.util.List;

import javax.management.MBeanServerConnection;
import javax.management.remote.JMXConnector;

import org.apache.commons.logging.Log;
import org.ow2.weblab.bundle.utils.PathUtils;
import org.ow2.weblab.bundle.utils.ProcessUtils;
import org.ow2.weblab.bundle.utils.ProcessUtils.Env;
import org.ow2.weblab.bundle.utils.Utils;

/**
 * An abstract class to manage Server in WebLab
 *
 * @author asaval
 *
 */
public abstract class WebLabServer {


	public static enum State {
		STARTING, STARTED, STOPPING, UNDEFINED, STOPPED
	}


	public static String START = "start";


	public static String RESTART = "restart";


	public static String STOP = "stop";


	public static String STATUS = "status";


	private int port, jmxPort, ajpPort, timeout;


	private String host = "localhost";


	protected Log logger = Utils.getLogger();


	/**
	 * Home directory
	 */
	private String home;


	/**
	 * Execution directory
	 */
	private String binDirectory;


	/**
	 * Server name
	 */
	private String name;


	/**
	 * Script to start/stop/request server
	 */
	private String script;


	/**
	 * Execution environment for this server
	 */
	private Env env;


	/**
	 * if false commands and requests will be discarded
	 */
	private boolean enabled;


	/**
	 * if false, when an errors at start, we can proceed with the next server. Otherwise, we have to fail and stop.
	 */
	private boolean mandatory;


	private List<String> cleanablePathsAfterStop;


	public WebLabServer() {

	}


	public String getHome() {

		return this.home;
	}


	public void setHome(final String home) {

		this.home = home;
	}


	public int getPort() {

		return this.port;
	}


	public void setPort(final int port) {

		this.port = port;
	}


	public int getJmxPort() {

		return this.jmxPort;
	}


	public void setJmxPort(final int jmxPort) {

		this.jmxPort = jmxPort;
	}


	public int getAjpPort() {

		return this.ajpPort;
	}


	public void setAjpPort(final int ajpPort) {

		this.ajpPort = ajpPort;
	}


	public int getTimeout() {

		return this.timeout;
	}


	public void setTimeout(final int timeout) {

		this.timeout = timeout;
	}


	public String getBinDirectory() {

		return this.binDirectory;
	}


	public void setBinDirectory(final String binDirectory) {

		this.binDirectory = binDirectory;
	}


	public String getName() {

		return this.name;
	}


	public void setName(final String name) {

		this.name = name;
	}


	public String getScript() {

		return this.script;
	}


	public void setScript(final String script) {

		this.script = script;
	}


	public Env getEnv() {

		return this.env;
	}


	public void setEnv(final Env env) {

		this.env = env;
	}


	public boolean isEnabled() {

		return this.enabled;
	}


	public void setEnabled(final boolean enabled) {

		this.enabled = enabled;
	}


	/**
	 * @return the mandatory
	 */
	public boolean isMandatory() {

		return this.mandatory;
	}


	/**
	 * @param mandatory
	 *            the mandatory to set
	 */
	public void setMandatory(final boolean mandatory) {

		this.mandatory = mandatory;
	}


	/**
	 * @param subDir
	 *            The name of the sub directory inside home
	 * @return Absolute path to sub directory
	 */
	public String getSubDir(final String subDir) {

		return PathUtils.createPath(this.home, subDir);
	}


	/**
	 * @return Something to be grepped in ps aux
	 */
	public abstract String getProcessIdentificationClue();


	/**
	 * Execute a command on the server
	 *
	 * @param env_p
	 *            environment to execute the command
	 * @param command
	 *            a command to execute
	 * @return the process executing the command
	 */
	public Process execute(final Env env_p, final String command) {

		if (!this.isEnabled()) {
			this.logger.info(this.getName() + " discarded command " + command + " since control is disabled.");
			return ProcessUtils.dummyProccess();
		}
		this.logger.debug("Executing command " + command + " on " + this.getName());

		String script_l = this.getScript();
		if (script_l == null) {
			// use default script
			final String ext = System.getProperty("os.name").contains("Windows") ? ".bat" : ".sh";
			script_l = PathUtils.createPath(this.getHome(), "bin") + "catalina" + ext;
		}
		return ProcessUtils.runProcess(this.logger, this.getBinDirectory(), env_p, script_l, command);
	}


	/**
	 * Return server process status
	 *
	 * @return server process status
	 */
	public State status() {

		State status = State.UNDEFINED;
		try {
			if (-1 == ProcessUtils.getProcessId(this.logger, this)) {
				status = State.STOPPED;
			} else {
				status = State.STARTED;
			}
		} catch (final Exception e) {
			this.logger.debug("Could not check " + this.getName() + " status: " + e.getMessage(), e);
		}
		this.logger.debug(this.getName() + " returned status : " + status);
		return status;
	}


	/**
	 * Return the pid of the WebLabServer using JMX
	 *
	 * @return the pid of the WebLabServer using JMX
	 */
	public int getPid() {

		int pid = -1;
		if (this.isEnabled() && (this.getJMXConnector() != null)) {
			try (JMXConnector connector = this.getJMXConnector()) {
				final MBeanServerConnection connection = connector.getMBeanServerConnection();
				final RuntimeMXBean bean = ManagementFactory.newPlatformMXBeanProxy(connection, ManagementFactory.RUNTIME_MXBEAN_NAME, RuntimeMXBean.class);
				final String name_l = bean.getName();
				final String[] ids = name_l.split("@");
				pid = Integer.parseInt(ids[0]);
			} catch (final Exception exception) {
				// may fail when checking before server is started
				this.logger.debug(exception.getMessage(), exception);
			}
		}
		return pid;
	}


	/**
	 * Return MemoryMXBean of the WebLabServer
	 *
	 * @return MemoryMXBean of the WebLabServer
	 */
	public MemoryMXBean monitorMemory() {

		MemoryMXBean memory = null;
		if (this.isEnabled()) {
			try (JMXConnector connector = this.getJMXConnector()) {
				final MBeanServerConnection connection = connector.getMBeanServerConnection();
				if (connection != null) {
					memory = ManagementFactory.newPlatformMXBeanProxy(connection, ManagementFactory.MEMORY_MXBEAN_NAME, MemoryMXBean.class);
				} else {
					this.logger.warn("Could not connect to " + this.getName());
				}
			} catch (final Exception exception) {
				this.logger.error(exception.getMessage(), exception);
			}
		}
		return memory;
	}


	/**
	 * Starts the server
	 *
	 * Optionally return the Process corresponding to the running server if possible
	 *
	 * Do NOT assume the server is not started if this method returns null value.
	 *
	 * @return the Process corresponding to the running server if possible else null
	 */
	public abstract Process start();


	/**
	 * Stops the server
	 */
	public abstract void stop();


	/**
	 * Check Server Status: try to connect on listening port and access JMX server to retrieve information about deployed webapps
	 *
	 * @param showDetails
	 *            log webapps status
	 * @return true is the server is started and all its webapps are started too.
	 */
	public abstract boolean isServerFullyStarted(boolean showDetails);


	/**
	 * Retrieve JMXConnector for java this server
	 *
	 * @return getJMXConnector for java this server
	 */
	public abstract JMXConnector getJMXConnector();


	public String supportedCommands() {

		return this.name + " [start|stop|restart|status] \t will start, stop and give information on " + this.name + " current status.\n";
	}


	/**
	 * @return the cleanablePathsAfterStop
	 */
	public List<String> getCleanablePathsAfterStop() {

		return this.cleanablePathsAfterStop;
	}


	/**
	 * @param cleanablePathsAfterStop
	 *            the cleanablePathsAfterStop to set
	 */
	public void setCleanablePathsAfterStop(final List<String> cleanablePathsAfterStop) {

		this.cleanablePathsAfterStop = cleanablePathsAfterStop;
	}


	/**
	 * @return the host
	 */
	public String getHost() {

		return this.host;
	}


	/**
	 * @param host
	 *            the host to set
	 */
	public void setHost(String host) {

		this.host = host;
	}

}
