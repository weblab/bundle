/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.bundle.utils.manager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;

/**
 * Windows process manager
 *
 * @author asaval
 *
 */
public class WindowsProcessManager extends ProcessManager {


	/**
	 * pattern to extract process info from tasklist command
	 */
	private final Pattern processPattern = Pattern.compile("^\"(.+)\",\"(.+)\",\"(.+)\",\"(.+)\",\"(.+)\"$");


	/**
	 * pattern to extract pid and port info from netstat command
	 */
	private final Pattern netstatPattern = Pattern.compile("^\\s+TCP\\s+\\d+\\.\\d+\\.\\d+\\.\\d+:(\\d+)\\s+\\d+\\.\\d+\\.\\d+\\.\\d+:\\d+\\s+LISTENING\\s+(\\d+)$");


	@Override
	protected String killCommand(final int pid, final boolean force) {

		return "taskkill /PID " + (force ? pid + " /F" : pid + "");
	}


	@Override
	protected ProcessBuilder getProcessCommand() {

		return new ProcessBuilder("tasklist", "/FO", "CSV", "/NH");
	}


	@Override
	protected ProcessBuilder getNetstatCommand() {

		return new ProcessBuilder("netstat", "-ano");
	}


	@Override
	protected ProcessInfo parseProcessInfo(final String line, final Log logger) {

		ProcessInfo procInfo = null;
		final Matcher matcher = this.processPattern.matcher(line);
		if (matcher.matches()) {
			final String pid = matcher.group(2);
			final String command = matcher.group(1);
			procInfo = new ProcessInfo(pid, command);
		} else {
			logger.warn("Could not find pid and command for process line: " + line);
		}
		return procInfo;
	}


	@Override
	protected Map<Integer, ProcessInfo> buildProcessInfo(final List<String> data) {

		final Map<Integer, ProcessInfo> portProcs = new HashMap<>();
		final Map<Integer, ProcessInfo> pidProcs = new HashMap<>();

		for (final String line : data) {
			final Matcher matcher = this.netstatPattern.matcher(line);
			if (matcher.matches()) {
				final int pid = Integer.parseInt(matcher.group(2));
				final int port = Integer.parseInt(matcher.group(1));

				// check if there already is a procinfo with this id
				ProcessInfo pInfo = pidProcs.get(Integer.valueOf(pid));
				if (pInfo == null) {
					// build it
					pInfo = new ProcessInfo(pid, null);
				}

				// update list
				pInfo.addPort(port);
				pidProcs.put(Integer.valueOf(pid), pInfo);
				portProcs.put(Integer.valueOf(port), pInfo);

			}
		}

		return portProcs;
	}


	@Override
	public void setPermission(final String path, final String permissions, final boolean recursive, final Log logger) {

		// nothing to do.
	}
}