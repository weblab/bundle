/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.bundle.utils.manager;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.ow2.weblab.bundle.server.WebLabServer;
import org.ow2.weblab.bundle.utils.ProcessUtils;

/**
 * Process manager interface
 *
 * @author asaval
 *
 */
public abstract class ProcessManager {


	/**
	 * Current ProcessManager
	 */
	private static ProcessManager current;


	/**
	 * Kill a process by its pid
	 *
	 * @param logger
	 *            a logger
	 * @param pid
	 *            pid of the process to kill
	 * @param force
	 *            if true, it send a terminate uncatchable signal, else send a nice kill signal catchable by the process
	 */
	public void kill(final Log logger, final int pid, final boolean force) {
		final Runtime rt = Runtime.getRuntime();
		try {
			final String killCommand = this.killCommand(pid, force);
			if (logger.isDebugEnabled()) {
				logger.debug("Sending " + (force ? "an uncatchable" : "a") + " kill signal to process " + pid + " : " + killCommand);
			}
			rt.exec(killCommand);
		} catch (final IOException ioe) {
			logger.error(ioe.getMessage(), ioe);
		}
	}


	/**
	 * Command to send kill signals to a process using its pid.
	 *
	 * @param pid
	 *            the pid of the process
	 * @param force
	 *            if true, it will directly terminate the process else it will send a catchable kill signal to the process.
	 * @return the command to kill the process with the given id.
	 */
	protected abstract String killCommand(final int pid, final boolean force);


	/**
	 * Processbuilder returning data describing processes and their id
	 *
	 * @return a processbuilder
	 */
	protected abstract ProcessBuilder getProcessCommand();


	/**
	 * Parse results from a command listing running process on the system
	 *
	 * @param line
	 *            a line containing os specific information about a process
	 * @param logger
	 *            a logger
	 * @return a ProcessInfo containing pid, command name and ports for the process described in the line
	 */
	protected abstract ProcessInfo parseProcessInfo(final String line, final Log logger);


	/**
	 * Processbuilder returning data describing processes, their ports and id
	 *
	 * @return a processbuilder
	 */
	protected abstract ProcessBuilder getNetstatCommand();


	/**
	 * Build a map of process available from their opened port.
	 *
	 * @param data
	 *            lines describing processes, their ports and id
	 * @return a map of processes with their port, pid and command name
	 */
	protected abstract Map<Integer, ProcessInfo> buildProcessInfo(final List<String> data);


	/**
	 * List java process and return a summary for each process
	 *
	 * @param logger
	 *            a logger
	 * @return the list of current processes
	 */
	public List<ProcessInfo> list(final Log logger) {
		final List<ProcessInfo> infos = new LinkedList<>();
		try {
			final List<String> data = ProcessUtils.execute(this.getProcessCommand(), logger);

			for (final String line : data) {
				final ProcessInfo procInfo = this.parseProcessInfo(line, logger);
				if (procInfo != null) {
					infos.add(procInfo);
				}
			}

		} catch (final Exception exception) {
			logger.error(exception.getMessage(), exception);
		}
		return infos;
	}


	/**
	 * Return the PID of the WebLabServer or -1 if the server is not started
	 *
	 * @param logger
	 *            a logger
	 * @param server
	 *            a WebLab Server
	 * @return the PID of the WebLabServer or -1 if the server is not started
	 */
	public int getPID(final Log logger, final WebLabServer server) {
		// retrieve process with opened ports

		int pid = -1;
		try {
			final List<String> data = ProcessUtils.execute(this.getNetstatCommand(), logger);
			final Map<Integer, ProcessInfo> process = this.buildProcessInfo(data);
			// we are checking opened port
			final Integer port = Integer.valueOf(server.getPort());
			final ProcessInfo info = process.get(port);
			if (info != null) {
				pid = info.getPid();
			} else {
				if (logger.isDebugEnabled()) {
					logger.debug(server.getName() + " does not listen on port " + port);
				}
			}
		} catch (final Exception exception) {
			logger.error(exception.getMessage(), exception);
		}

		if (logger.isDebugEnabled()) {
			logger.debug("Process id for server " + server.getName() + " is : " + pid);
		}
		return pid;
	}


	/**
	 * Return ProcessManager instance depending on the operating system
	 *
	 * @return a ProcessManager
	 */
	public static ProcessManager getInstance() {
		if (ProcessManager.current != null) {
			return ProcessManager.current;
		}
		ProcessManager processManager = null;
		if (System.getProperty("os.name").toLowerCase().indexOf("windows") > -1) {
			processManager = new WindowsProcessManager();
		} else {
			processManager = new LinuxProcessManager();
		}
		if (ProcessManager.current == null) {
			ProcessManager.current = processManager;
		}
		return processManager;
	}


	/**
	 * Set permissions on a directory/file
	 *
	 * @param path
	 *            path to the file/directory
	 * @param permissions
	 *            permissions to set
	 * @param recursive
	 *            if true and path refers to a directory, it will apply permission on sub-directory too. Else permissions will be set on path only
	 * @param logger
	 *            a logger
	 */
	public abstract void setPermission(String path, String permissions, boolean recursive, Log logger);
}