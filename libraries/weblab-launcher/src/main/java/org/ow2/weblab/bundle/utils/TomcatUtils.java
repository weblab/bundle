/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.bundle.utils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.ow2.weblab.bundle.conf.WebLabBean;
import org.ow2.weblab.bundle.server.TomcatServer;

public class TomcatUtils {


	private static final int TO_SHUTDOWN = 72;


	private static final int TO_REDIRECT = 38;


	private TomcatUtils() {
	}


	/**
	 * Update Tomcat server ports and return a tomcatServer
	 *
	 * @param tomcat
	 *            The original tomcat server to be used as basis
	 * @param newTomcat
	 *            The location of the new tomcat server
	 * @param tomcatName
	 *            The name of the new tomcat server
	 * @param index
	 *            The value to add to jmx port and http port to original tomcat value to create the new tomcat
	 * @param logger
	 *            The logger used inside
	 * @param configuration
	 *            The configuration bean to be used as basis
	 * @return a TomcatServer
	 * @throws IOException
	 *             If an error occured compying the files
	 */
	public static TomcatServer updatePorts(final TomcatServer tomcat, final File newTomcat, final String tomcatName, final int index, final Log logger, final WebLabBean configuration)
			throws IOException {
		// selecting ports
		final int port = tomcat.getPort() + index;
		final int jmxPort = tomcat.getJmxPort() + index;
		final int shutdownPort = port - TomcatUtils.TO_SHUTDOWN;
		final int redirPort = port - TomcatUtils.TO_REDIRECT;

		final String serverConf = PathUtils.createPath(PathUtils.getWebLabHome("conf"), "samples") + "server.xml";
		final String newServerConf = PathUtils.createPath(newTomcat.getAbsolutePath(), "conf") + "server.xml";
		// copy in tomcat
		FileUtils.copyFile(new File(serverConf), new File(newServerConf));

		// replacing parameters
		final Map<String, String> portsReplacements = new HashMap<>();
		portsReplacements.put("$SHUTDOWN$", Integer.toString(shutdownPort));
		portsReplacements.put("$PORT$", Integer.toString(port));
		portsReplacements.put("$REDIRECT_PORT$", Integer.toString(redirPort));

		Utils.update(logger, portsReplacements, newServerConf);

		logger.info("Tomcat Server " + tomcatName + " will listen on (standard, jmx and shutdown) ports: " + port + ", " + jmxPort + " and " + shutdownPort + " located in "
				+ newTomcat.getAbsolutePath());

		return TomcatUtils.buildTomcat(tomcat, tomcatName, newTomcat.getAbsolutePath(), port, jmxPort, shutdownPort, redirPort, logger, configuration);
	}


	/**
	 * Return a list of non-standard directory deployed in tomcat
	 *
	 * @param tomcat
	 *            a tomcat Server
	 * @param logger
	 *            a logger
	 * @return the list of non -standard directory deployed in the tomcat server
	 */
	public static List<String> listServices(final TomcatServer tomcat, final Log logger) {
		final File webapps = new File(PathUtils.createPath(tomcat.getHome(), "webapps"));
		final File[] fileServices = webapps.listFiles();
		final List<String> services = new LinkedList<>();
		if (fileServices == null) {
			logger.error("Could not list services from " + webapps.getAbsolutePath());
			return services;
		}

		for (final File file : fileServices) {
			final String name = file.getName();
			if (file.isDirectory() && (name != null) && !name.equals("ROOT") && !name.equals("manager")) {
				logger.debug("Adding service " + name + " in list");
				services.add(name);
			}
		}
		return services;
	}


	/**
	 * Return a Tomcat
	 *
	 * @param tomcat The old tomcat server
	 * @param name The name of the new tomcat server
	 * @param home The new tomcat server home
	 * @param port The new port of the tomcat server
	 * @param jmxPort The new jmx port of the tomcat server
	 * @param shutdown The new shutdown port of the tomcat server
	 * @param redirect The new redirect port of the tomcat server
	 * @param logger The logger used inside
	 * @param configuration not used
	 * @return a Tomcat server
	 */
	public static TomcatServer buildTomcat(final TomcatServer tomcat, final String name, final String home, final int port, final int jmxPort, final int shutdown, final int redirect,
			final Log logger, final WebLabBean configuration) {
		// building new Tomcat
		final TomcatServer newOne = new TomcatServer();
		newOne.setName(name);
		newOne.setHome(home);
		newOne.setPort(port);
		newOne.setJmxPort(jmxPort);
		newOne.setBinDirectory(tomcat.getBinDirectory());

		// TODO: update and fix
		// newOne.setStopEnv(Env.prepare("CATALINA_HOME", PathUtils.removeLastSeparator(newOne.getHome()), // CATALINA HOME
		// "JAVA_OPTS", configuration.getProperty(Constants.Option.TOMCAT_OPTS)));
		// newOne.setEnv(Env.prepare("CATALINA_HOME", PathUtils.removeLastSeparator(newOne.getHome()), // CATALINA HOME
		// "JAVA_OPTS", configuration.getProperty(Constants.Option.TOMCAT_OPTS)+
		// " -Dweblab.repository="+configuration.getProperty(Constants.Option.WEBLAB_REPOSITORY)+
		// " "+Constants.Default.getTomcatJMXOptions(jmxPort)));
		newOne.setScript(PathUtils.createPath(newOne.getHome(), "bin") + "catalina." + PathUtils.getExtension());
		newOne.setTimeout(tomcat.getTimeout());
		newOne.setEnabled(true);
		return newOne;
	}

}
