/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.bundle.server;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import javax.management.remote.JMXConnector;

import org.ow2.weblab.bundle.utils.PathUtils;
import org.ow2.weblab.bundle.utils.ProcessUtils;
import org.ow2.weblab.bundle.utils.ProcessUtils.Env;
import org.ow2.weblab.bundle.utils.Utils;

/**
 * Represents a TomcatServer
 *
 * @author asaval
 *
 */
public class TomcatServer extends WebLabServer {


	public static class DisabledServer extends TomcatServer {


		public DisabledServer(final String name) {
			super.setName("Disabled " + name);
			this.setEnabled(false);
			this.setMandatory(false);
			this.setHome("");
		}


		@Override
		public Process start() {
			return ProcessUtils.dummyProccess();
		}

	}


	private Env stopEnv;


	private TomcatJMXClient tjmxc;


	public TomcatServer() {
	}


	public Env getStopEnv() {
		return this.stopEnv;
	}


	public void setStopEnv(final Env stopEnv) {
		this.stopEnv = stopEnv;
	}


	/**
	 * Returns a TomcatJMXClient if it already exists, else creates it.
	 *
	 * @param timeout
	 *            a timeout before creation fail
	 * @return a TomcatJMXClient
	 * @throws IOException
	 *             If something wrong occured creating the client
	 */
	protected TomcatJMXClient getJMXClient(final int timeout) throws IOException {
		if (this.tjmxc == null) {
			this.tjmxc = new TomcatJMXClient(this.logger, this.getJmxPort(), timeout);
		}
		return this.tjmxc;
	}


	@Override
	public Process start() {
		return this.start("start");
	}


	@Override
	public void stop() {
		this.stop("stop");
	}


	/**
	 * Starts the Tomcat server
	 *
	 * @param command
	 *            the strat command (strat/run)
	 * @return the process starting the server
	 */
	public Process start(final String command) {
		if (!this.isEnabled()) {
			this.logger.info(this.getName() + " discarded command " + command + " since control is disabled.");
			return ProcessUtils.dummyProccess();
		}
		// check status
		if (this.status() != State.STOPPED) {
			this.logger.error(this.getName() + " is not stopped ! Aborting start...");
			return null;
		}

		// check pid file
		if (ProcessUtils.checkPidFile(this.logger, this, false)) {
			// server already started; we abort.
			this.logger.error(this.getName() + " already started ! Aborting ...");
			return null;
		}

		this.logger.info(this.getName() + " is starting ...");
		return this.execute(this.getEnv(), command);
	}


	/**
	 * Stops the Tomcat server
	 *
	 * @param command
	 *            the stop command
	 * @return the process stoping the server
	 */
	public Process stop(final String command) {
		if (!this.isEnabled()) {
			this.logger.info(this.getName() + " discarded command " + command + " since control is disabled.");
			return ProcessUtils.dummyProccess();
		}
		this.logger.info(this.getName() + " is stopping ...");
		return this.execute(this.getStopEnv(), command);
	}


	/**
	 * Ask the server if it is ready until the timeout
	 *
	 * @return true if the server is completely ready else false
	 */
	public boolean becomeReadyOrDieTrying() {
		if (!this.isEnabled()) {
			this.logger.info("Can not check if " + this.getName() + " is ready since control is disabled.");
			return true;
		}
		this.logger.debug("Checking " + this.getName() + " status and its webapps status.");
		boolean ready = false;
		try {
			// check status
			ready = this.getJMXClient(this.getTimeout()).waitForServerStart(this.getTimeout());
		} catch (final Exception exception) {
			this.logger.error(exception.getLocalizedMessage(), exception);
		}
		if (ready) {
			// create pid file
			ProcessUtils.createPids(this.logger, this);
		}

		return ready;
	}


	@Override
	public boolean isServerFullyStarted(final boolean showDetails) {
		if (!this.isEnabled()) {
			this.logger.info("Can not check if " + this.getName() + " is started since control is disabled.");
			return true;
		}
		this.logger.info("Checking " + this.getName() + " status ... ");
		boolean started = true;
		// check if something is running on the given port
		if (!Utils.isPortAvailable(this.logger, this.getHost(), this.getPort()) || ((this.getAjpPort() > 0) && !Utils.isPortAvailable(this.logger, this.getHost(), this.getAjpPort()))) {
			// check webapps
			try {
				final Map<String, String> webapps = this.getJMXClient(this.getTimeout()).getWebAppStatus();
				for (final String context : webapps.keySet()) {
					final String status = webapps.get(context);
					final String result = "\tWeb application " + context + " on " + this.getName() + " is " + status;
					if (!"STARTED".equals(status)) {
						started = false;
						this.logger.warn(result);
					} else if (showDetails) {
						this.logger.info(result);
					} else {
						if (this.logger.isDebugEnabled()) {
							this.logger.debug(result);
						}
					}
				}
			} catch (final IOException e) {
				this.logger.error(e.getLocalizedMessage());
				this.logger.debug(e.getLocalizedMessage(), e);
				this.logger.info(this.getName() + " is started on port " + this.getPort() + " but information about deployed web applications is not available on jmx port " + this.getJmxPort() + ".");
				started = false;
			}
		} else {
			if (this.getAjpPort() > 0) {
				this.logger.info(this.getName() + " is not started. Cannot reach port " + this.getPort() + " nor AJP port " + this.getAjpPort() + ".");
			} else {
				this.logger.info(this.getName() + " is not started on port " + this.getPort() + ".");
			}
			started = false;
		}
		return started;
	}


	/**
	 * Allows remote commands execution on webapps
	 *
	 * @param webapp
	 *            a webapp name
	 * @param command
	 *            the command to execute on the webapp
	 */
	public void executeOnWebApp(final String webapp, final String command) {
		this.logger.info("[" + this.getName() + "] Executing command " + command + " on " + webapp);
		try {
			final TomcatJMXClient client = this.getJMXClient(this.getTimeout());
			final String action = command;
			if (WebLabServer.RESTART.equals(action)) {
				this.executeOnWebApp(webapp, WebLabServer.STOP);
				this.executeOnWebApp(webapp, WebLabServer.START);
			} else if ("stats".equals(action)) {
				this.getJMXClient(3).showStatistics();
			} else {
				this.logger.info(client.execute(webapp, action));
			}
		} catch (final IOException e) {
			this.logger.error(e.getLocalizedMessage(), e);
		}
	}


	@Override
	public String getProcessIdentificationClue() {
		return "-Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=" + this.getJmxPort();
	}


	@Override
	public JMXConnector getJMXConnector() {
		return ProcessUtils.createJMXclient(this.getHost(), this.getJmxPort(), "", "", "jmxrmi", 2000, false, this.logger);
	}


	/**
	 * Return the file to the service configuration
	 *
	 * @param service
	 *            The name of the webapp
	 * @param cxfFileName
	 *            The name of the cxf file to be overridden
	 * @return file to the service configuration
	 */
	public File getServiceConfigurationFile(final String service, final String cxfFileName) {
		return new File(PathUtils.createPath(this.getSubDir("webapps"), service, "WEB-INF") + cxfFileName);
	}


	@Override
	public String supportedCommands() {
		return this.getName() + " [start|stop|restart|status|[webapp start|stop|restart|stats]] \tlast command allows to start/stop any deployed [webapp].\n";
	}

}
