/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.bundle.utils.manager;

import java.util.HashSet;
import java.util.Set;

/**
 * Process info stub
 *
 * @author asaval
 *
 */
public class ProcessInfo {


	private String command;


	private final int pid;


	private final Set<Integer> ports = new HashSet<>();


	public ProcessInfo(final String pid, final String command) {
		this.pid = Integer.parseInt(pid);
		this.command = command;
	}


	public ProcessInfo(final int pid, final String command) {
		this.pid = pid;
		this.command = command;
	}


	public void setCommand(final String command) {
		this.command = command;
	}


	public String getCommand() {
		return this.command;
	}


	public int getPid() {
		return this.pid;
	}


	public void addPort(final int port) {
		this.ports.add(Integer.valueOf(port));
	}


	public Set<Integer> getports() {
		return this.ports;
	}


	@Override
	public String toString() {
		return this.pid + " " + this.command + " " + this.ports;
	}

}