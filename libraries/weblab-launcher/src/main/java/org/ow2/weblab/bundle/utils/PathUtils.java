/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.bundle.utils;

import java.io.File;

/**
 * Utility class to manage Pathes, set as deprecated once we use Java 1.7
 *
 * @author asaval
 *
 */
public final class PathUtils {


	private PathUtils() {
	}


	/**
	 * Current directory
	 */
	private static String HOME = ".";


	public static void setHome(final String home) {
		PathUtils.HOME = home;
	}


	/**
	 * Retrieve WebLab Home
	 *
	 * @param subdir
	 *            The name of the sub directory inside weblab home
	 * @return WebLab Home
	 */
	public static String getWebLabHome(final String subdir) {
		final File home = new File(PathUtils.HOME);
		return PathUtils.createPath(home.getAbsolutePath(), subdir);
	}


	/**
	 * Get script extension depending on OS bat for Windows and sh for others
	 *
	 * @return the OS depending script extension
	 */
	public static String getExtension() {
		return System.getProperty("os.name").contains("Windows") ? "bat" : "sh";
	}


	/**
	 * Create a path with OS dedicated path separator
	 *
	 * @param waypoints
	 *            waypoints on the path
	 * @return a string representing the path for the current OS
	 */
	public static String createPath(final String... waypoints) {
		final StringBuffer buffer = new StringBuffer();
		if (waypoints != null) {
			for (final String waypoint : waypoints) {
				if (!"".equals(waypoint)) {
					buffer.append(waypoint);
					if (!waypoint.endsWith(File.separator)) {
						buffer.append(File.separatorChar);
					}
				}
			}
		}

		String path = buffer.toString();
		path = path.replace(File.separator + "." + File.separator, File.separator);
		return path;
	}


	/**
	 * Remove last separator at the end of the path if any
	 *
	 * @param dir
	 *            a path
	 * @return the path without a separator at the end
	 */
	public static String removeLastSeparator(final String dir) {
		String result = dir;
		if (result.endsWith(File.separator)) {
			result = result.substring(0, dir.length() - 1);
		}
		return result;
	}


	/**
	 * Return only the name of the file (without any extension)
	 *
	 * @param file
	 *            a file
	 * @return name of the file
	 */
	public static String name(final File file) {
		String name = file.getName();
		final int index = file.getName().lastIndexOf('.');
		if (index != -1) {
			name = file.getName().substring(0, index);
		}
		return name;
	}



}