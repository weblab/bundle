/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.bundle.utils.manager;

import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.ow2.weblab.bundle.server.WebLabServer;
import org.ow2.weblab.bundle.utils.ProcessUtils;

/**
 * Linux/MacOS process manager
 *
 * @author asaval
 *
 */
public class LinuxProcessManager extends ProcessManager {


	private final Pattern processPattern = Pattern.compile("^(\\d+) (.+)$");


	private static final String LAUNCHER_COMMAND = "org.ow2.weblab.bundle.Launcher";


	@Override
	protected String killCommand(final int pid, final boolean force) {

		return "kill " + (force ? "-9 " + pid : "-15 " + pid);
	}


	@Override
	public int getPID(final Log logger, final WebLabServer server) {

		// this method is faster and give more information than checking ports
		final List<ProcessInfo> list = this.list(logger);
		final String clue = server.getProcessIdentificationClue();
		if (clue == null) {
			logger.warn("No process identification clue for " + server.getName());
			return -1;
		}
		int pid = -1;
		for (final ProcessInfo proc : list) {
			// do not consider current launcher process
			if (proc.getCommand().contains(LinuxProcessManager.LAUNCHER_COMMAND)) {
				continue;
			}
			if (proc.getCommand().contains(clue)) {
				if (pid == -1) {
					pid = proc.getPid();
				} else {
					logger.warn("There are at least two instances of WebLab " + server.getName() + " (pids:  " + pid + " and" + proc.getPid()
							+ ") running at the same time, please check if this is the intended behaviour.");
				}
			}
		}
		if (logger.isDebugEnabled()) {
			logger.debug("Process id for server " + server.getName() + " is : " + pid);
		}
		return pid;
	}


	@Override
	protected ProcessBuilder getProcessCommand() {

		return new ProcessBuilder("/bin/sh", "-c", "ps axww | grep java | grep -v grep");
	}


	@Override
	protected ProcessBuilder getNetstatCommand() {

		return new ProcessBuilder("netstat", "-anp");
	}


	@Override
	protected ProcessInfo parseProcessInfo(final String line, final Log logger) {

		if (logger.isTraceEnabled()) {
			logger.trace("Parsing process info line: " + line);
		}

		final String trimmedLine = line.trim();

		ProcessInfo procInfo = null;
		final Matcher matcher = this.processPattern.matcher(trimmedLine);
		if (matcher.matches()) {
			final String pid = matcher.group(1);
			final String command = matcher.group(2);
			procInfo = new ProcessInfo(pid, command);
			if (logger.isTraceEnabled()) {
				logger.trace("processus info: " + "(" + procInfo.getPid() + ") " + procInfo);
			}
		} else {
			logger.warn("Could not find pid and command for process line: " + trimmedLine);
		}

		return procInfo;
	}


	@Override
	protected Map<Integer, ProcessInfo> buildProcessInfo(final List<String> data) {

		throw new RuntimeException("\"buildProcessInfo\" method is not implemented in LinuxProcessManager since there are more efficient ways to retrieve information on process.");
	}


	@Override
	public void setPermission(final String path, final String permissions, final boolean recursive, final Log logger) {

		try {
			if (logger.isTraceEnabled()) {
				logger.trace("Setting permissions " + permissions + " on " + path);
			}
			ProcessUtils.execute(new ProcessBuilder("chmod", "-R", permissions, path), logger);
		} catch (final Exception exception) {
			logger.error(exception.getMessage(), exception);
		}
	}

}