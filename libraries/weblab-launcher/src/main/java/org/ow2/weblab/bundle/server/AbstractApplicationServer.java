/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.bundle.server;

import org.ow2.weblab.bundle.utils.ProcessUtils;

/**
 * @author lmartin
 */
public abstract class AbstractApplicationServer extends WebLabServer {


	/**
	 * Get stop script.
	 *
	 * @return Return stop script.
	 */
	protected abstract String[] getStopScript();


	/**
	 * Get start script.
	 *
	 * @return Return start script.
	 */
	protected abstract String[] getStartScript();


	@Override
	public Process start() {
		if (!this.isEnabled()) {
			this.logger.info(this.getName() + " discarded command " + this.getName() + " since control is disabled.");
			return ProcessUtils.dummyProccess();
		}

		// check if configured
		this.checkConfiguration();

		this.logger.info(this.getName() + " is starting ...");

		return ProcessUtils.runProcess(this.logger, this.getBinDirectory(), this.getEnv(), this.getStartScript());
	}


	@Override
	public void stop() {
		this.logger.info(this.getName() + " is stopping ...");

		if (this.getStopScript() != null) {
			ProcessUtils.runProcess(this.logger, this.getBinDirectory(), this.getEnv(), this.getStopScript());
		}
	}


	/**
	 *
	 */
	protected void checkConfiguration() {
		// Nothing to do
	}

}
