/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.bundle.server;


import java.io.IOException;
import java.io.StringReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.management.remote.JMXConnector;
import javax.xml.bind.JAXBContext;
import javax.xml.transform.stream.StreamSource;

import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.ssl.SSLContextBuilder;
import org.ow2.weblab.bundle.utils.ProcessUtils;
import org.ow2.weblab.bundle.utils.Utils;
import org.ow2.weblab.bundle.utils.heritrix.Engine;
import org.ow2.weblab.bundle.utils.heritrix.Job;

/**
 * Manage a Heritrix server
 *
 * @author asaval
 */
public class Heritrix extends WebLabServer {


	private String uiHost;


	private String commandLineArguments;


	private String user;


	private String password;


	public Heritrix() {
	}


	@Override
	public String getProcessIdentificationClue() {
		return this.getHome();
	}


	@Override
	public boolean isServerFullyStarted(final boolean showDetails) {
		if (!this.isEnabled()) {
			this.logger.info("Can not check if " + this.getName() + " is started since control is disabled.");
			return true;
		}
		if (!Utils.isPortAvailable(this.logger, this.getHost(), this.getPort())) {
			if (showDetails) {
				try (final CloseableHttpClient client = this.createClient()) {
					final HttpGet httpget = new HttpGet(new URL("https", this.getUiHost(), this.getPort(), "/engine").toURI());
					httpget.setHeader("Accept", "application/xml");
					final String result = client.execute(httpget, new BasicResponseHandler());
					this.logger.debug(result);

					final Engine engine = JAXBContext.newInstance(Engine.class).createUnmarshaller().unmarshal(new StreamSource(new StringReader(result)), Engine.class).getValue();
					for (final Job job : engine.jobs) {
						this.logger.info("\tJob '" + job.shortName + "' is currently in status '" + job.statusDescription + "'. It has already been launched " + job.launchCount + " time(s).");
					}

				} catch (final Exception e) {
					this.logger.error("An error occured trying to talk with WebUI.", e);
				}
			}
			return true;
		}
		return false;
	}


	@Override
	public Process start() {
		this.logger.info("Starting " + this.getName());
		if (!this.isEnabled()) {
			this.logger.info(this.getName() + " not started since control is disabled.");
			return ProcessUtils.dummyProccess();
		}
		// check status
		if (this.status() != State.STOPPED) {
			this.logger.error(this.getName() + " is not stopped ! Aborting start...");
			return null;
		}

		// check pid file
		if (ProcessUtils.checkPidFile(this.logger, this, false)) {
			// server already started; we abort.
			this.logger.error(this.getName() + " already started ! Aborting ...");
			return null;
		}

		final String command = this.getScript().trim() + " " + this.commandLineArguments.trim();
		this.logger.info(this.getName() + " is starting ...");
		if (!this.isEnabled()) {
			this.logger.info(this.getName() + " discarded commmand " + command + " since control is disabled.");
			return ProcessUtils.dummyProccess();
		}

		return ProcessUtils.runProcess(this.logger, this.getBinDirectory(), this.getEnv(), command.split(" "));
	}


	@Override
	public void stop() {
		// Talking to WebUI of Heritrix to stop it
		try (final CloseableHttpClient client = this.createClient()) {

			// posting the shutdown request
			final HttpPost httppost = new HttpPost(new URL("https", this.getUiHost(), this.getPort(), "/engine").toURI());

			// Add your data
			final List<NameValuePair> nameValuePairs = new ArrayList<>(2);
			nameValuePairs.add(new BasicNameValuePair("I'm sure", "on"));
			nameValuePairs.add(new BasicNameValuePair("action", "Exit Java Process"));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			final String result = client.execute(httppost, new BasicResponseHandler());
			this.logger.debug(result);

			this.logger.info(this.getName() + " stopped");
		} catch (final URISyntaxException urise) {
			this.logger.error("Wrong URL format when talking to Heritrix WebUI.", urise);
		} catch (final ClientProtocolException cpe) {
			this.logger.error("Wrong protocol while talking to Heritrix WebUI.", cpe);
		} catch (final IOException ioe) {
			this.logger.error("An error occured while talking to Heritrix WebUI.", ioe);
		} catch (final KeyManagementException kme) {
			this.logger.error("Unable to create SSL context to talk with WebUI.", kme);
		} catch (final NoSuchAlgorithmException nsae) {
			this.logger.error("Unable to create SSL context to talk with WebUI.", nsae);
		} catch (final KeyStoreException kse) {
			this.logger.error("Unable to create SSL context to talk with WebUI.", kse);
		}
	}



	/**
	 * Execute a command
	 *
	 * @param command
	 *            The name of the command called
	 * @return The started process description (if any)
	 */
	public Process execute(final String command) {
		if (!this.isEnabled()) {
			this.logger.info(this.getName() + " discarded command " + command + " since control is disabled.");
			return ProcessUtils.dummyProccess();
		}
		if ("stop".equals(command) || "restart".equals(command)) {
			// stop
			this.stop();
		}
		if ("start".equals(command) || "restart".equals(command)) {
			// start
			return this.start();
		}
		return null;
	}


	public String getUiHost() {
		return this.uiHost;
	}


	public void setUiHost(final String uiHost) {
		this.uiHost = uiHost;
	}


	public String getUser() {
		return this.user;
	}


	public void setUser(final String user) {
		this.user = user;
	}


	public String getPassword() {
		return this.password;
	}


	public void setPassword(final String password) {
		this.password = password;
	}


	public String getCommandLineArguments() {
		return this.commandLineArguments;
	}


	public void setCommandLineArguments(final String commandLineArguments) {
		this.commandLineArguments = commandLineArguments;
	}


	@Override
	public JMXConnector getJMXConnector() {
		return null;
	}


	private CloseableHttpClient createClient() throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
		final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
		credentialsProvider.setCredentials(new AuthScope(this.getUiHost(), this.getPort()), new UsernamePasswordCredentials(this.user, this.password));
		final SSLConnectionSocketFactory sslSocketFactory = new SSLConnectionSocketFactory(new SSLContextBuilder().loadTrustMaterial(null, new TrustSelfSignedStrategy() {


			@Override
			public boolean isTrusted(final java.security.cert.X509Certificate[] chain, final String authType) {
				return true;
			}
		}).build(), NoopHostnameVerifier.INSTANCE);
		return HttpClients.custom().setDefaultCredentialsProvider(credentialsProvider).setSSLSocketFactory(sslSocketFactory).setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE).build();
	}


}
