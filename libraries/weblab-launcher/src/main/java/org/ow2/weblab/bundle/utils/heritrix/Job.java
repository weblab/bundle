/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.bundle.utils.heritrix;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This class is inspired from heritrix3-wrapper project.
 *
 * It represents the result of a JAXB unmarshaling of Heritrix answer when calling the engine page.
 *
 * @author ymombrun
 */
@XmlRootElement
public class Job {


	@XmlElement(required = true)
	public String shortName;


	@XmlElement(required = true)
	public String url;


	@XmlElement(required = true)
	public Boolean isProfile;


	@XmlElement(required = true)
	public Integer launchCount;


	@XmlElement(required = true)
	public String lastLaunch;


	@XmlElement(required = true)
	public Boolean hasApplicationContext;


	@XmlElement(required = true)
	public String statusDescription;


	@XmlElement(required = true)
	public Boolean isLaunchInfoPartial;


	@XmlElement(required = true)
	public String primaryConfig;


	@XmlElement(required = true)
	public String primaryConfigUrl;


	@XmlElement(required = false)
	public String crawlControllerState;


	@XmlElement(required = false)
	public String crawlExitStatus;


	@XmlElement(required = true)
	public String key;


	/**
	 * Default empty contructor
	 */
	public Job() {
		super();
	}

}
