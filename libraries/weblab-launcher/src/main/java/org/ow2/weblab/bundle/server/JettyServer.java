/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.bundle.server;

import java.io.File;

import javax.management.remote.JMXConnector;

import org.apache.commons.io.FileUtils;
import org.ow2.weblab.bundle.utils.ProcessUtils;
import org.ow2.weblab.bundle.utils.Utils;

/**
 * An specific implementation of WebLab server to be used in order to manager a new server into the main script.
 *
 * @author vcorjon
 */
public class JettyServer extends WebLabServer {


	private static final String JETTY_PID = "jetty.pid";


	/**
	 * Just an empty constructor that says that the manager has been created.
	 */
	public JettyServer() {

		super();
		this.logger.debug("JettyServer started.");
	}



	@Override
	public int getPid() {

		if (this.isLocked()) {
			// Server has already been started at least one time. Checking if it is really running (file is not deleted in case of dirty stop).
			if (Utils.isPortAvailable(this.logger, this.getHost(), this.getPort())) {
				this.logger.error("Port " + this.getPort() + " is available but lock file " + this.getLockFile() + " exists. Strange!");
				return -1;
			}
			final File pidFile = this.getLockFile();
			try {
				return Integer.valueOf(FileUtils.readFileToString(pidFile).trim()).intValue();
			} catch (final Exception e) {
				final String msg = "Unable to get pid from file " + pidFile + ".";
				this.logger.error(msg);
				this.logger.debug(msg, e);
			}
		}
		return -1;
	}



	private File getLockFile() {

		return new File(this.getHome(), JettyServer.JETTY_PID);
	}


	private boolean isLocked() {

		final File lockFile = this.getLockFile();
		return lockFile.exists() && (lockFile.length() > 0);
	}


	@Override
	public String getProcessIdentificationClue() {

		this.logger.debug("getProcessIdentificationClue is not implemented by JettyServer.");
		return this.getHome();
	}



	@Override
	public boolean isServerFullyStarted(final boolean showDetails) {

		if (!this.isEnabled()) {
			this.logger.info("Can not check if " + this.getName() + " is started since control is disabled.");
			return true;
		}
		if ((this.status() == State.STARTED) && !Utils.isPortAvailable(this.logger, this.getHost(), this.getPort())) {
			this.logger.info(this.getName() + " is started.");
			if (showDetails) {
				// this.logger.info("showDetails is not implemented.");
			}
			return true;
		}
		return false;
	}



	/**
	 * Execute a command
	 *
	 * @param command
	 *            The string command to execute (start, stop or status)
	 * @return The process description
	 */
	public Process execute(final String command) {

		if (!this.isEnabled()) {
			this.logger.info(this.getName() + " discarded command " + command + " since control is disabled.");
			return ProcessUtils.dummyProccess();
		}
		if ("start".equals(command)) {
			return this.start();
		} else if ("stop".equals(command)) {
			this.stop();
			return ProcessUtils.dummyProccess();
		} else if ("status".equals(command)) {
			this.logger.info(this.getName() + " getting status...");
		} else if ("restart".equals(command)) {
			this.stop();
			return this.start();
		} else {
			this.logger.warn("Command " + command + " is not handled by " + this.getName());
			return null;
		}
		return this.execute(this.getEnv(), command);
	}


	@Override
	public JMXConnector getJMXConnector() {

		return null;
	}


	@Override
	public Process start() {

		// check status
		if (this.status() != State.STOPPED) {
			this.logger.error(this.getName() + " is not stopped! Aborting start...");
			return null;
		}

		// check pid file
		if (ProcessUtils.checkPidFile(this.logger, this, false)) {
			// server already started; we abort.
			this.logger.error(this.getName() + " already started! Aborting...");
			return null;
		}

		if (this.isLocked()) {
			this.logger.info(this.getName() + " has not been stopped correctly. Removing lock file.");
			final File lockFile = this.getLockFile();
			if (!lockFile.delete()) {
				this.logger.error("Unable to remove lock file " + lockFile + ". Please try to remove it manually. Aborting...");
				return null;
			}

		}

		this.logger.info(this.getName() + " is starting...");
		final Process p = this.execute(this.getEnv(), "start");
		final int exitCode;
		try {
			exitCode = p.waitFor();
		} catch (final InterruptedException ie) {
			this.logger.info(this.getName() + " start failed." + ie.getLocalizedMessage());
			return null;
		}
		if ((exitCode == 0) && (this.status() == State.STARTED)) {
			this.logger.info(this.getName() + " is started.");
		} else {
			this.logger.info(this.getName() + " start failed. ExitCode=" + String.valueOf(exitCode) + ", status=" + this.status());
			return null;
		}
		return p;
	}



	@Override
	public void stop() {

		this.logger.info(this.getName() + " is stopping ...");
		this.execute(this.getEnv(), "stop");
		this.logger.info(this.getName() + " is stopped.");
	}

}
