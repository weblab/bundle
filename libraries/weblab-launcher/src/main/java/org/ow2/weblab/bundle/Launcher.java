/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.bundle;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.common.SolrException;
import org.ow2.weblab.bundle.conf.WebLabBean;
import org.ow2.weblab.bundle.server.ActiveMQServer;
import org.ow2.weblab.bundle.server.Bus;
import org.ow2.weblab.bundle.server.ServerManager;
import org.ow2.weblab.bundle.server.SolrServer;
import org.ow2.weblab.bundle.server.TomcatServer;
import org.ow2.weblab.bundle.server.WebLabServer;
import org.ow2.weblab.bundle.server.ZooKeeperServer;
import org.ow2.weblab.bundle.utils.PathUtils;
import org.ow2.weblab.bundle.utils.ProcessUtils;
import org.ow2.weblab.bundle.utils.ProcessUtils.Env;
import org.ow2.weblab.bundle.utils.Utils;
import org.ow2.weblab.bundle.utils.manager.ProcessManager;
import org.ow2.weblab.components.solr.utils.SolrUtils;
import org.springframework.util.CollectionUtils;

/**
 * WebLab Launcher to launch and stop servers
 *
 * @author asaval
 *
 */
public class Launcher {


	private static final String STATUS = "status";


	private static final String ZOOKEEPER = "zookeeper";


	private static final String SOLR = "solr";


	private static final String RESTART = "restart";


	private static final String STOP = "stop";


	private static final String START = "start";


	private static final Log logger = Utils.getLogger();


	/**
	 * Options
	 */
	public static WebLabBean configuration;


	/**
	 * @param args
	 *            The main command arguments
	 */
	public static void main(final String[] args) {

		// set weblab.home system property if not already defined
		if (System.getProperty("weblab.home") == null) {
			System.setProperty("weblab.home", PathUtils.getWebLabHome(""));
			if (Launcher.logger.isDebugEnabled()) {
				Launcher.logger.debug("weblab.home system property not set, setting it at : " + PathUtils.getWebLabHome(""));
			}
		}
		// set conf and data dir
		System.setProperty("weblab.conf", PathUtils.getWebLabHome("conf"));
		System.setProperty("weblab.data", PathUtils.getWebLabHome("data"));

		try {
			// load configuration
			Launcher.configuration = Utils.loadConfiguration(Launcher.logger);
		} catch (final Exception exception) {
			Launcher.logger.error("Could not load configuration file. Make sure you neither make a mistake in configuration.xml nor forget to provide external libraries.", exception);
			Launcher.end(Constants.COMMAND_FAILED);
		}

		// check if it is the first launch
		Launcher.initChecks();

		if (Launcher.logger.isDebugEnabled()) {
			Launcher.logger.debug("Given args: " + args.length + " " + Arrays.asList(args));
		}

		if (args.length < 1) {
			Launcher.end(Constants.COMMAND_FAILED);
		}
		final String commandOrServerName = args[0];

		String command2 = "";
		if (args.length > 1) {
			command2 = args[1];
		}

		final boolean force = Launcher.parseArgs(args, "--force");

		if (Launcher.logger.isDebugEnabled()) {
			Launcher.logger.debug("Loading  ServerManager using configuration file");
		}
		final ServerManager serverManager = new ServerManager(Launcher.logger, Launcher.configuration);

		final Map<String, String> serverCommands = serverManager.getServersCommands();

		// start all
		if (Launcher.START.equals(commandOrServerName)) {
			Launcher.check();
			serverManager.start(Utils.getServer(Launcher.logger, args));
		} // end all
		else if (Launcher.STOP.equals(commandOrServerName)) {
			serverManager.stop(Utils.getServer(Launcher.logger, args), force);
		} // Restart all
		else if (Launcher.RESTART.equals(commandOrServerName)) {
			serverManager.stop(Utils.getServer(Launcher.logger, args), force);
			Launcher.check();
			serverManager.start(Utils.getServer(Launcher.logger, args));
		} // server specific command
		else if (serverCommands.keySet().contains(commandOrServerName)) {
			if (Launcher.START.equals(command2)) {
				Launcher.check();
				serverManager.start(commandOrServerName);
			} else if (Launcher.STOP.equals(command2)) {
				serverManager.stop(commandOrServerName, force);
			} else if (Launcher.RESTART.equals(command2)) {
				serverManager.stop(commandOrServerName, force);
				serverManager.start(commandOrServerName);
			} else if ("".equals(command2) || Launcher.STATUS.equals(command2)) {
				serverManager.status(commandOrServerName);
			} else {
				serverManager.rmi(commandOrServerName, command2, args);
			}
		} else if (Launcher.SOLR.equals(commandOrServerName) && (Launcher.STOP.equals(command2) || Launcher.START.equals(command2) || Launcher.RESTART.equals(command2))) {
			if (Launcher.START.equals(command2)) {
				Launcher.startAllSolr();
			} else if (Launcher.STOP.equals(command2)) {
				Launcher.stopAllSolr();
			} else if (Launcher.RESTART.equals(command2)) {
				Launcher.stopAllSolr();
				Launcher.startAllSolr();
			}
		} else if (Launcher.ZOOKEEPER.equals(commandOrServerName) && (Launcher.STOP.equals(command2) || Launcher.START.equals(command2) || Launcher.RESTART.equals(command2))) {
			if (Launcher.START.equals(command2)) {
				Launcher.startAllZoo();
			} else if (Launcher.STOP.equals(command2)) {
				Launcher.stopAllZoo();
			} else if (Launcher.RESTART.equals(command2)) {
				Launcher.stopAllZoo();
				Launcher.startAllZoo();
			}
		} else if ("details".equals(commandOrServerName)) {
			Launcher.help(true, serverCommands);
		} else if ("help".equals(commandOrServerName) || "--help".equals(commandOrServerName)) {
			Launcher.help(false, null);
		} else if ("cidre".equals(commandOrServerName)) {
			Utils.reinit(Launcher.logger);
		} else if ("reset".equals(commandOrServerName)) {
			Launcher.logger.info("Before resetting, stopping everything...");
			serverManager.stop(null, force);
			Launcher.logger.info("Before resetting, starting only ActiveMQ, Zookeeper and Solr servers when enabled...");
			Launcher.startAMQ();
			Launcher.startAllZoo();
			Launcher.startAllSolr();
			Launcher.reset();
			Launcher.logger.info("Stopping everything....");
			serverManager.stop(null, force);
			Launcher.logger.info("Start everything...");
			serverManager.start(null);
		} else if ("status".equals(commandOrServerName)) {
			serverManager.status(null);
		} else {
			Launcher.end(Constants.COMMAND_FAILED);
		}

		Launcher.end(0);
	}


	private static void startAMQ() {

		final ActiveMQServer activeMQServer = Launcher.configuration.getActiveMQServer();
		if ((activeMQServer == null) || !activeMQServer.isEnabled()) {
			Launcher.logger.debug("No need to start ActiveMQ before reset. It is not available.");
		}
		ServerManager.start(activeMQServer, Launcher.logger, true);
	}


	private static void startAllSolr() {

		for (final SolrServer solrServer : Launcher.getSolrServers()) {
			solrServer.start();
		}
	}


	private static void stopAllSolr() {

		for (final SolrServer solrServer : Launcher.getSolrServers()) {
			solrServer.stop();
		}
	}


	private static ArrayList<SolrServer> getSolrServers() {

		final ArrayList<SolrServer> solrServers = new ArrayList<>();
		for (final WebLabServer server : Launcher.configuration.getServers()) {
			if ((server instanceof SolrServer) && server.isEnabled()) {
				solrServers.add((SolrServer) server);
			}
		}
		return solrServers;
	}


	private static void startAllZoo() {

		for (final ZooKeeperServer zooServer : Launcher.getZooServers()) {
			zooServer.start();
		}
	}


	private static void stopAllZoo() {

		for (final ZooKeeperServer zooServer : Launcher.getZooServers()) {
			zooServer.stop();
		}
	}


	private static ArrayList<ZooKeeperServer> getZooServers() {

		final ArrayList<ZooKeeperServer> zooServers = new ArrayList<>();
		for (final WebLabServer server : Launcher.configuration.getServers()) {
			if ((server instanceof ZooKeeperServer) && server.isEnabled()) {
				zooServers.add((ZooKeeperServer) server);
			}
		}
		return zooServers;
	}


	/**
	 * Return true if a command is contained in args list
	 *
	 * @param args
	 *            list of arguments
	 * @param command
	 *            a command
	 * @return true if the command is contained in args list
	 */
	private static boolean parseArgs(final String[] args, final String command) {

		for (final String arg : args) {
			if (command.equals(arg)) {
				return true;
			}
		}
		return false;
	}


	/**
	 * Delete indexes, repository and webdav
	 */
	private static void reset() {


		// Solr data if enabled.
		final ArrayList<SolrServer> solrServers = Launcher.getSolrServers();
		final ArrayList<ZooKeeperServer> zooServers = Launcher.getZooServers();
		if (solrServers.isEmpty() || zooServers.isEmpty()) {
			Launcher.logger.debug("No SOLR/ZOOKEEPER servers. No need to reset.");
		} else {
			Launcher.logger.info("Before resetting, check status of Solr and Zookeeper servers.");

			// solr index
			HashMap<String, HashMap<String, String>> initCollections = null;
			CloudSolrClient solrClient = null;
			try {
				for (final SolrServer server : solrServers) {
					initCollections = server.getInitCollections();
					if (!CollectionUtils.isEmpty(initCollections)) {
						solrClient = server.getSolrClient();
						break;
					}
				}

				// we found some initial collections solr instances are working with -> reset them
				if ((initCollections != null) && !initCollections.isEmpty()) {

					// is solr correctly running?
					boolean needRestart = false;
					for (final Entry<String, HashMap<String, String>> collection : initCollections.entrySet()) {
						final String collectionName = collection.getKey();
						try {
							if (!SolrUtils.checkLeaders(solrClient, collectionName)) {
								needRestart = true;
								break;
							}
						} catch (final SolrException e) {
							Launcher.logger.warn(e.getMessage() + " while checking solr status -> will try to restart it");
							needRestart = true;
							break;
						}
					}

					// restart if needed
					if (needRestart) {

						// stop all solr nodes
						for (final SolrServer server : solrServers) {
							server.stop();
						}
						// stop all zookeeper nodes
						for (final ZooKeeperServer server : zooServers) {
							server.stop();
						}
						// start all zookeeper nodes
						for (final ZooKeeperServer server : zooServers) {
							server.start();
						}
						// start all solr nodes
						for (final SolrServer server : solrServers) {
							server.start();
						}
					}

					// then delete all collections
					for (final Entry<String, HashMap<String, String>> collection : initCollections.entrySet()) {
						final String collectionName = collection.getKey();
						Launcher.logger.info("deleting solr collection : " + collectionName);
						try {
							SolrUtils.deleteCollection(solrClient, collectionName, 30);
						} catch (final Exception e) {
							Launcher.logger.error("An error occured deleting collection " + collectionName + " from Solr.", e);
						}
					}
				}
			} finally {
				if (solrClient != null) {
					try {
						solrClient.close();
					} catch (final IOException ioe) {
						Launcher.logger.debug("An error occured closing solr client", ioe);
					}
				}
			}
		}


		// ActiveMQ
		final ActiveMQServer activeMQ = Launcher.configuration.getActiveMQServer();
		if ((activeMQ == null) || !activeMQ.isEnabled()) {
			Launcher.logger.debug("No ActiveMQ configured. No need to clear the queues.");
		} else {
			Launcher.logger.info("Clear the ActiveMQ queues if needed.");
			activeMQ.purge(null);
		}


		// Repository
		final String repositoryPath = Launcher.configuration.getRepository();
		if (repositoryPath == null) {
			Launcher.logger.debug("No repository found. Nothing to delete from repository.");
		} else {
			Launcher.logger.info("Clear repository of processed data.");
			try {
				Launcher.cleanFolderIfExist(repositoryPath);
			} catch (final IOException ioe) {
				Launcher.logger.warn("An error occured cleaning repository.", ioe);
			}
		}


		// Content
		final String contentFolder = Launcher.configuration.getContentFolder();
		if (contentFolder == null) {
			Launcher.logger.debug("No content folder path found. Nothing to delete.");
		} else {
			Launcher.logger.info("Clear content folder.");
			try {
				Launcher.cleanFolderIfExist(contentFolder);
			} catch (final IOException ioe) {
				Launcher.logger.warn("An error occurred cleaning content directory", ioe);
			}
		}


		// Webdav
		final String webdavURL = Launcher.configuration.getWebdavUrl();
		if (webdavURL == null) {
			Launcher.logger.debug("No webDAV access found. Nothing to delete from webDAV repository.");
		} else if (Launcher.configuration.getPortal().isServerFullyStarted(false)) {
			final String user = Launcher.configuration.getWebdavUser();
			final String password = Launcher.configuration.getWebdavPassword();
			if (Utils.resetWebDav(Launcher.logger, webdavURL, user, password)) {
				Launcher.logger.info(Launcher.configuration.getName() + " stored data has been reset.");
			}
		} else {
			Launcher.logger.warn(Launcher.configuration.getPortal().getName() + " server not started: can not delete webDAV repository.");
		}

		final String resetScript = Launcher.configuration.getResetScript();
		if (resetScript == null) {
			Launcher.logger.debug("No additional reset script to execute.");
		} else {
			final File script = new File(resetScript);
			if (script.exists()) {
				Launcher.logger.info("Executing additional reset script");
				ProcessUtils.runProcess(Launcher.logger, script.getParent(), Env.defaut(), (System.getProperty("os.name").contains("Windows") ? "" : "./") + script.getName());
			} else {
				Launcher.logger.error("Additional reset script provided does not exists at " + script.getAbsolutePath() + " based on provided " + resetScript + ".");
			}
		}
	}


	/**
	 * @param repositoryPath
	 *            THe location of the repository folder to be cleaned
	 * @throws IOException
	 *             If an error occured cleaning the target folder
	 */
	protected static void cleanFolderIfExist(final String repositoryPath) throws IOException {

		final File repository = new File(repositoryPath);
		if (repository.exists()) {
			FileUtils.cleanDirectory(repository);
			Launcher.logger.info(repository.getAbsolutePath() + " has been reset.");
		} else {
			Launcher.logger.info(repository.getAbsolutePath() + " does not exist, nothing to reset.");
		}
	}


	/**
	 * Alert user on those parameters
	 */
	private static void check() {

		final int cores = Runtime.getRuntime().availableProcessors();
		if (cores < 2) {
			Launcher.logger.warn(
					"Warning: there is only 1 processor available. This could lead to issues because of the Service Oriented Architecture of the " + Launcher.configuration.getName() + " Platform.");
			Launcher.logger.warn("Warning: do not use this configuration for prodution use.");
		}

		final long maxMemory = Runtime.getRuntime().maxMemory();
		if (maxMemory < 1500000000) {
			Launcher.logger.warn("Warning: Maximum memory is " + maxMemory + " bytes. It could lead to issues when running " + Launcher.configuration.getName() + "; some server might not start.");
		}
	}


	/**
	 * Check if it is the first run of the launcher by checking if the the weblab.log file exist
	 * and check if JRE supports class version de spec
	 */
	private static void initChecks() {

		final File pidsFile = new File(PathUtils.createPath(PathUtils.getWebLabHome("data"), "pids"));
		if (!pidsFile.exists()) {
			// run processmanager to set rights to teh specified servers
			final ProcessManager procManager = ProcessManager.getInstance();
			procManager.setPermission(PathUtils.getWebLabHome("conf"), "u+rx", true, Launcher.logger);
			procManager.setPermission(PathUtils.getWebLabHome("data"), "u+rwx", true, Launcher.logger);
			final TomcatServer applicationServer = Launcher.configuration.getApplicationServer();
			if ((applicationServer != null) && applicationServer.isEnabled() && (applicationServer.getBinDirectory() != null)) {
				procManager.setPermission(applicationServer.getBinDirectory(), "u+rx", true, Launcher.logger);
			}
			final TomcatServer portal = Launcher.configuration.getPortal();
			if ((portal != null) && portal.isEnabled() && (portal.getBinDirectory() != null)) {
				procManager.setPermission(portal.getBinDirectory(), "u+rx", true, Launcher.logger);
			}
			final Bus bus = Launcher.configuration.getBus();
			if ((bus != null) && bus.isEnabled() && (bus.getBinDirectory() != null)) {
				procManager.setPermission(bus.getBinDirectory(), "u+rx", true, Launcher.logger);
			}
			final ActiveMQServer activeMQ = Launcher.configuration.getActiveMQServer();
			if ((activeMQ != null) && activeMQ.isEnabled() && (activeMQ.getBinDirectory() != null)) {
				procManager.setPermission(activeMQ.getBinDirectory(), "u+rx", true, Launcher.logger);
			}
			for (final WebLabServer server : Launcher.configuration.getServers()) {
				if ((server != null) && server.isEnabled() && (server.getBinDirectory() != null)) {
					procManager.setPermission(server.getBinDirectory(), "u+rx", true, Launcher.logger);
				}
			}
			final String[] potentialExecutables = new String[] { Launcher.configuration.getResetScript(), PathUtils.getWebLabHome("weblab.sh"), PathUtils.getWebLabHome("weblab.bat"),
					PathUtils.getWebLabHome("weblab-launcher.jar") };
			for (final String exec : potentialExecutables) {
				if (exec != null) {
					procManager.setPermission(exec, "u+rx", true, Launcher.logger);
				}
			}

		}

		// check JVM version, stop if JVM does not expected class version spec
		if (Float.parseFloat(System.getProperty("java.class.version")) < Launcher.configuration.getClassVersion()) {
			Launcher.logger.warn("The current JVM is not compatible with required java class version: " + Launcher.configuration.getClassVersion()
					+ ", please upgrade your installation.\nProcessing chains and GUI might not work properly, do you want to start WebLab anyway? (Yes/No, default=No )");
			try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
				final String answer = br.readLine();
				if ("Yes".equalsIgnoreCase(answer)) {
					return;
				}
			} catch (final IOException ioe) {
				Launcher.logger.debug(ioe.getLocalizedMessage(), ioe);
			}
		}

	}


	/**
	 * Fail message
	 *
	 * @param code
	 *            The exit code. If not 0, then its an error.
	 */
	public static void end(final int code) {

		if (code != 0) {
			if (Launcher.configuration == null) {
				// something went wrong during configuration phase
				Launcher.configuration = new WebLabBean();
			}
			Launcher.help(false, null);
		}
		System.exit(code);
	}


	private static void help(final boolean details, final Map<String, String> serversCommands) {

		if (details) {
			String serversDoc = "";
			for (final String doc : serversCommands.values()) {
				serversDoc += "    " + doc;
			}

			Launcher.logger.info(Launcher.configuration.getName() + " Launcher must be invoked with one of the following command:\n" + "    start  [server]? \tStarts "
					+ Launcher.configuration.getName() + " or the provided server.\n" + "    stop   [server]? \tStops " + Launcher.configuration.getName() + " or the provided server.\n"
					+ "    status [server]? \tShows " + Launcher.configuration.getName() + " status or the provided server status.\n" + serversDoc
					+ "    reset \t\tReset all stored data: repository, index, webdav.\n" + "    details \t\tShows this message.\n" + "    help \t\tShows short version usage message.\n" + "\n"
					+ " available [server] values are: " + serversCommands.keySet() + ".");
		} else {
			Launcher.logger.info(Launcher.configuration.getName() + " Launcher must be invoked with one of the following command:\n" + "    start \t\tStarts " + Launcher.configuration.getName()
					+ " Bundle.\n" + "    stop \t\tStops " + Launcher.configuration.getName() + " Bundle.\n" + "    status \t\tShows " + Launcher.configuration.getName() + " Bundle status.\n"
					+ "    help \t\tShows this message.\n" + "    details \t\tShows more details.");
		}
	}


	private Launcher() {

	}

}
