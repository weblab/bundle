/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space SAS
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.bundle.server;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.stream.Stream;

import javax.management.remote.JMXConnector;

import org.apache.http.client.HttpClient;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.impl.HttpClientUtil;
import org.ow2.weblab.bundle.utils.PathUtils;
import org.ow2.weblab.bundle.utils.ProcessUtils;
import org.ow2.weblab.bundle.utils.Utils;
import org.ow2.weblab.components.solr.utils.SolrUtils;
import org.springframework.util.CollectionUtils;

/**
 * Solr server
 *
 * @author lmartin
 *
 */
public class SolrServer extends AbstractApplicationServer {


	private HashMap<String, String> startOptions;


	private HashMap<String, String> stopOptions;


	private String zookeeperEnsemble;


	private CloudSolrClient solrClient;


	private ArrayList<String> collectionConfigs;


	private String identificationClue;


	HashMap<String, HashMap<String, String>> initCollections;


	private String dataDirectory;


	private static final String ZKHOST = "-zkhost";


	private static final String CMD = "-cmd";


	private static final String ZKCLI = "zkcli";


	private static final String CONFDIR = "-confdir";


	private static final String CONFNAME = "-confname";


	private static final String UPCONFIG = "upconfig";


	public SolrServer() {
		// Empty
	}


	@Override
	protected String[] getStartScript() {
		return this.getScriptWithOpts(WebLabServer.START, this.startOptions);
	}


	@Override
	protected String[] getStopScript() {
		return this.getScriptWithOpts(WebLabServer.STOP, this.stopOptions);
	}


	protected String[] getScriptWithOpts(final String command, final Map<String, String> opts) {
		final String ext = System.getProperty("os.name").contains("Windows") ? ".cmd" : "";
		if (!opts.isEmpty()) {
			final ArrayList<String> commandLine = new ArrayList<>();
			commandLine.add(PathUtils.createPath(this.getHome(), "bin") + "solr" + ext);
			commandLine.add(command);
			for (final Entry<String, String> opt : opts.entrySet()) {
				commandLine.add(opt.getKey());
				commandLine.add(opt.getValue());
			}
			return commandLine.toArray(new String[commandLine.size()]);
		}
		return new String[] { PathUtils.createPath(this.getHome(), "bin") + "solr" + ext, command };
	}


	@Override
	public JMXConnector getJMXConnector() {
		return null;
	}


	@Override
	public String getProcessIdentificationClue() {
		return this.identificationClue;
	}


	public void setIdentificationClue(final String identificationClue) {
		this.identificationClue = identificationClue;
	}


	@Override
	public boolean isServerFullyStarted(final boolean showDetails) {
		return !Utils.isPortAvailable(this.logger, this.getHost(), this.getPort());
	}


	/**
	 * @return the startOptions
	 */
	public HashMap<String, String> getStartOptions() {
		return this.startOptions;
	}


	/**
	 * @param startOptions
	 *            the startOptions to set
	 */
	public void setStartOptions(final HashMap<String, String> startOptions) {
		this.startOptions = startOptions;
	}


	/**
	 * @return the stopOptions
	 */
	public HashMap<String, String> getStopOptions() {
		return this.stopOptions;
	}


	/**
	 * @param stopOptions
	 *            the stopOptions to set
	 */
	public void setStopOptions(final HashMap<String, String> stopOptions) {
		this.stopOptions = stopOptions;
	}


	/**
	 * @return the zookeeperEnsemble
	 */
	public String getZookeeperEnsemble() {
		return this.zookeeperEnsemble;
	}


	/**
	 * @param zookeeperEnsemble
	 *            the zookeeperEnsemble to set
	 */
	public void setZookeeperEnsemble(final String zookeeperEnsemble) {
		this.zookeeperEnsemble = zookeeperEnsemble;
		final HttpClient httpClient = HttpClientUtil.createClient(null);
		HttpClientUtil.setSoTimeout(httpClient, 300000);
		HttpClientUtil.setConnectionTimeout(httpClient, 5000);
		this.solrClient = new CloudSolrClient.Builder().withHttpClient(httpClient).withZkHost(zookeeperEnsemble).build();
	}


	/**
	 * Check that configuration is OK and store it in shared zookeeper server
	 */
	@Override
	protected void checkConfiguration() {
		if (this.zookeeperEnsemble == null) {
			this.logger.debug("No zookeeper ensemble defined. No solr configuration uploaded on zookeeper.");
			return;
		}

		final Path cloudScriptsDir = Paths.get(this.getHome(), "server", "scripts", "cloud-scripts");
		if (!cloudScriptsDir.toFile().exists()) {
			this.logger.error("No solr cloud scripts directory found. Could not upload solr configuration to zookeeper");
			return;
		}

		// add solr.xml file on zookeeper server
		this.uploadSolrXML(cloudScriptsDir);

		// upload solr configurations of each collection on zookeeper server
		if (!CollectionUtils.isEmpty(this.collectionConfigs)) {
			for (final String config : this.collectionConfigs) {
				final Path localConfDir = Paths.get(this.getHome(), "..", "conf", "solr", "cores", config);
				this.uploadCollectionConfig(cloudScriptsDir, localConfDir, config);
			}
		}
	}


	// upload solr.xml configuration file on zookeeper server
	private void uploadSolrXML(final Path cloudScriptsDir) {
		final Path solrConfFile = Paths.get(this.getHome(), "..", "conf", "solr", "solr.xml");
		final String zkFirstHost = this.zookeeperEnsemble.split(",")[0];
		if (solrConfFile.toFile().exists()) {
			final String ext = System.getProperty("os.name").contains("Windows") ? ".bat" : ".sh";
			try {
				ProcessUtils.runProcess(this.logger, cloudScriptsDir.toString(), this.getEnv(), cloudScriptsDir.resolve(SolrServer.ZKCLI + ext).toString(), SolrServer.ZKHOST, zkFirstHost, SolrServer.CMD,
						"putfile", "/solr.xml", solrConfFile.toString());
			} catch (final Exception e) {
				this.logger.error("Could not upload solr.xml configuration file on zookeeper server", e);
			}
		} else {
			this.logger.error("Could not find solr.xml configuration file : " + solrConfFile.toString());
		}
	}


	// upload a collection configuration file on zookeeper and link this uploaded configuration to specified collections
	private void uploadCollectionConfig(final Path cloudScriptsDir, final Path localConfDir, final String configName) {
		// upload and link collection configuration on zookeeper host
		if (localConfDir.toFile().exists()) {
			final String ext = System.getProperty("os.name").contains("Windows") ? ".bat" : ".sh";
			try {
				final String zkFirstHost = this.zookeeperEnsemble.split(",")[0];
				ProcessUtils.runProcess(this.logger, cloudScriptsDir.toString(), this.getEnv(), cloudScriptsDir.resolve(SolrServer.ZKCLI + ext).toString(), SolrServer.CMD, SolrServer.UPCONFIG,
						SolrServer.ZKHOST, zkFirstHost, SolrServer.CONFNAME, configName, SolrServer.CONFDIR, localConfDir.toString());
			} catch (final Exception e) {
				this.logger.error("Could not upload solr configuration for logbooks on zookeeper server", e);
			}
		}
	}


	@Override
	public Process start() {

		// normal start
		final Process p = super.start();

		// there is a list of collections to create/init
		if (!CollectionUtils.isEmpty(this.initCollections)) {

			// test if collection directory already exist locally
			final boolean localCollec = this.collectionLocalDirExists();

			// a collection local directory already exist -> do not create collections again
			if (localCollec) {
				return p;
			}

			// wait for solr start-up
			try {
				p.waitFor(60, TimeUnit.SECONDS);
			} catch (final InterruptedException e) {
				this.logger.error("Could not wait for initial start script before creating collections", e);
			}

			// get list of currently existing collection
			try {
				List<String> existingCollections;
				try {
					existingCollections = SolrUtils.getCollections(this.solrClient);
				} catch (final Exception e) {
					this.logger.debug("Error accessing collections. Retrying one second later before ignoring it.");
					Thread.yield();
					Thread.sleep(1000);
					existingCollections = SolrUtils.getCollections(this.solrClient);
				}
				if (existingCollections != null) {
					for (final Entry<String, HashMap<String, String>> collection : this.initCollections.entrySet()) {
						final String collectionName = collection.getKey();
						if (!existingCollections.contains(collectionName)) {
							final String configName = collection.getValue().get("config");
							final String shards = collection.getValue().get("shards");
							final String routerField = collection.getValue().get("routerField");
							int numReplicas = 1;
							try {
								numReplicas = Integer.parseInt(collection.getValue().get("numReplicas"));
							} catch (NullPointerException | NumberFormatException ignored) {
								this.logger.debug("numReplicas property is either not set or not valid.", ignored);
							}
							int maxShardPerNode = 3;
							try {
								maxShardPerNode = Integer.parseInt(collection.getValue().get("maxShardPerNode"));
							} catch (NullPointerException | NumberFormatException ignored) {
								this.logger.debug("maxShardPerNode property is either not set or not valid.", ignored);
							}
							SolrUtils.createCollection(this.solrClient, collectionName, configName, shards, routerField, numReplicas, maxShardPerNode, 300);
							this.logger.info("Creating new solr collection: "+collectionName);
						}
					}
				}
			} catch (final Exception e) {
				this.logger.error("Error while retrieving or creating inital collections in solr", e);
			} finally {
				try {
					this.solrClient.close();
				} catch (final IOException ioe) {
					this.logger.error("error while closing solr client", ioe);
				}
			}
		}
		return p;
	}


	protected boolean collectionLocalDirExists() {
		final Path localDataDirectory = Paths.get(this.dataDirectory);
		try (Stream<Path> subDir = Files.list(localDataDirectory)) {
			final Predicate<Path> predicate = new Predicate<Path>() {


				@Override
				public boolean test(final Path t) {
					for (final String collection : SolrServer.this.initCollections.keySet()) {
						if ((t != null) && (t.getFileName().toString().contains(collection))) {
							return true;
						}
					}
					return false;
				}
			};
			// a collection directory already exist locally -> do not create collection in solr
			if (subDir.anyMatch(predicate)) {
				this.logger.debug("At least one collection directory is present locally. do not create any collection.");
				return true;
			}
		} catch (final IOException e) {
			this.logger.error("Could not determine if collections directory exist locally", e);
			// could not check if collection dir exist locally -> consider collection as already existing
			return true;
		}
		return false;
	}


	/**
	 * Get list of collection configurations that will be uploaded in zookeeper ensemble when server will start
	 *
	 * @return {@link ArrayList} list of collection configurations that will be uploaded in zookeeper ensemble when server will start. Config name in zookeeper
	 *         will be the same as config local sub directory
	 */
	public ArrayList<String> getCollectionConfigs() {
		return this.collectionConfigs;
	}


	/**
	 * Set list of collection configurations that will be uploaded in zookeeper ensemble when server will start
	 *
	 * @param collectionConfigs
	 *            {@link ArrayList} list of collection configurations that will be uploaded in zookeeper ensemble when server will start. Config
	 *            name in zookeeper will be the same as config local sub directory
	 */
	public void setCollectionConfigs(final ArrayList<String> collectionConfigs) {
		this.collectionConfigs = collectionConfigs;
	}


	/**
	 * Set list of collections to initialize on instance startup
	 *
	 * @param initCollections
	 *            {@link HashMap} list of collections to initalize on instance startup
	 */
	public void setInitCollections(final HashMap<String, HashMap<String, String>> initCollections) {
		this.initCollections = initCollections;
	}

	/**
	 * Get list of collections to initialize on instance startup
	 *
	 * @return initCollections
	 *            {@link HashMap} list of collections to initalize on instance startup
	 */
	public HashMap<String, HashMap<String, String>> getInitCollections() {
		return this.initCollections;
	}
	
	/**
	 * Get solr client used by this server
	 * @return {@link CloudSolrClient} solr client used by this server
	 */
	public CloudSolrClient getSolrClient() {
		return this.solrClient;
	}
	
	/**
	 * @return the dataDirectory
	 */
	public String getDataDirectory() {
		return this.dataDirectory;
	}


	/**
	 * @param dataDirectory
	 *            the dataDirectory to set
	 */
	public void setDataDirectory(final String dataDirectory) {
		this.dataDirectory = dataDirectory;
	}

}
