/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.bundle.server;

import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.management.AttributeList;
import javax.management.ObjectInstance;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;

import org.apache.commons.logging.Log;
import org.ow2.weblab.bundle.server.WebLabServer.State;
import org.ow2.weblab.bundle.utils.ProcessUtils;
import org.ow2.weblab.bundle.utils.Utils;

/**
 * A JMX client with utility methods to access Apache Tomcat information about webapps
 *
 * @author asaval
 *
 */
public class TomcatJMXClient {


	private final Log logger;


	/**
	 * MBean connection on Tomcat
	 */
	public JMXConnector connection;


	/**
	 * JMX Client accessing JMX server on the given port
	 *
	 * @param logger
	 *            a logger
	 * @param jmxport
	 *            a jmx port for a tomcat server
	 * @param timeout
	 *            This client will retry connection on JMX remote server until timeout
	 * @throws IOException If any wrong occurs
	 */
	public TomcatJMXClient(final Log logger, final int jmxport, final int timeout) throws IOException {
		this.logger = logger;
		if (logger.isDebugEnabled()) {
			this.logger.debug("Connecting JMX client on port " + jmxport);
		}
		this.connection = ProcessUtils.createJMXclient("localhost", jmxport, "", "", "jmxrmi", timeout, true, logger);
		if (this.connection == null) {
			throw new IOException("Could not connect to remote JMX Server on port " + jmxport);
		}
		if (logger.isDebugEnabled()) {
			this.logger.debug("JMX Connection client on port " + jmxport + " succeed.");
		}
	}


	/**
	 * Retrieve webapp status for deploy web apps
	 *
	 * @return status for each web app
	 * @throws IOException If any wrong occurs
	 */
	public Map<String, String> getWebAppStatus() throws IOException {

		final Map<String, String> webapps = new TreeMap<>();

		if (this.logger.isDebugEnabled()) {
			this.logger.debug("Querying all Mbeans on remote jmx server.");
		}
		final Set<ObjectInstance> objects = this.connection.getMBeanServerConnection().queryMBeans(null, null);

		// list all beans
		for (final ObjectInstance oi : objects) {
			// select manager ones
			if ("Manager".equals(oi.getObjectName().getKeyProperty("type"))) {
				final Set<ObjectInstance> ois = this.connection.getMBeanServerConnection().queryMBeans(oi.getObjectName(), null);
				// check state for each context
				for (final ObjectInstance o : ois) {
					final String context = o.getObjectName().getKeyProperty("context");
					try {
						final Object status = this.connection.getMBeanServerConnection().getAttribute(o.getObjectName(), "stateName");
						webapps.put(context, status.toString());
						if (this.logger.isDebugEnabled()) {
							this.logger.debug("Context " + context + " is " + status);
						}
					} catch (final Exception e) {
						this.logger.error(e.getLocalizedMessage(), e);
					}
				}
			}
		}

		return webapps;
	}


	/**
	 * Wait for some time for server start
	 *
	 * @param timeout
	 *            in ms
	 * @return true if server is started before the end of the timeout else false
	 */
	public boolean waitForServerStart(final long timeout) {
		final Set<String> webapps = new HashSet<>();

		boolean status = this.status() == State.STARTED;
		final long time = System.currentTimeMillis();
		if (this.logger.isDebugEnabled()) {
			this.logger.debug("Waiting for server start " + timeout + "ms.");
		}
		while (!status && ((System.currentTimeMillis() - time) < timeout)) {
			status = this.status() == State.STARTED;
			if (!status) {
				try {
					Thread.sleep(1000);
					final Map<String, String> current = this.getWebAppStatus();
					for (final String key : current.keySet()) {
						if (!webapps.contains(key)) {
							webapps.add(key);
							this.logger.info("Application " + key + " is " + current.get(key));
						}
					}
				} catch (final Exception e) {
					this.logger.error(e.getLocalizedMessage(), e);
				}

			}
		}
		return status;
	}


	/**
	 * Return server status
	 *
	 * @return the server status (STARTING, STARTED, STOPPING or UNDEFINED)
	 */
	public State status() {
		State status = State.UNDEFINED;

		try {
			status = State.valueOf(this.connection.getMBeanServerConnection().getAttribute(ObjectName.getInstance("Catalina:type=Server"), "stateName").toString());
		} catch (final Exception e) {
			if (this.logger.isDebugEnabled()) {
				this.logger.debug(e.getLocalizedMessage(), e);
			}
		}

		return status;
	}


	/**
	 * Display performance statistics on services
	 */
	public void showStatistics() {
		try {
			final Set<ObjectInstance> set = this.connection.getMBeanServerConnection().queryMBeans(new ObjectName("org.apache.cxf:*"), null);
			final String[] attributes = new String[] { "AvgResponseTime", "MaxResponseTime", "MinResponseTime", "NumInvocations", "NumLogicalRuntimeFaults", "NumRuntimeFaults",
					"NumCheckedApplicationFaults", "NumUnCheckedApplicationFaults" };
			this.logger
					.info("Service, Operation, AvgResponseTime, MaxResponseTime, MinResponseTime, NumInvocations, NumLogicalRuntimeFaults, NumRuntimeFaults, NumCheckedApplicationFaults, NumUnCheckedApplicationFaults");
			for (final ObjectInstance oi : set) {
				final String operation = oi.getObjectName().getKeyProperty("operation");
				if ("org.apache.cxf.management.counters.ResponseTimeCounter".equals(oi.getClassName()) && (operation != null)) {
					String line = oi.getObjectName().getKeyProperty("port") + "," + operation;
					final AttributeList atl = this.connection.getMBeanServerConnection().getAttributes(oi.getObjectName(), attributes);
					for (int i = 0; i < attributes.length; i++) {
						String value = atl.get(i).toString().trim();
						value = value.split("=")[1].trim();
						line += "," + value;
					}
					this.logger.info(line);
				}
			}
		} catch (final Exception e) {
			if (this.logger.isDebugEnabled()) {
				this.logger.debug("Could not fetch statistics from JMX info " + e.getLocalizedMessage(), e);
			}
		}
	}


	/**
	 * Execute a remote method on a MBean
	 *
	 * @param webapp
	 *            the name of the webapp
	 * @param command
	 *            the method name to execute
	 * @return the String value returned by the call of toString() on the Object returned by the method or "No result returned.".
	 */
	public String execute(final String webapp, final String command) {
		String result = null;
		try {
			final ObjectName on = ObjectName.getInstance("Catalina:j2eeType=WebModule,name=//localhost/" + webapp + ",J2EEApplication=none,J2EEServer=none");

			final Object value = this.connection.getMBeanServerConnection().invoke(on, command, new Object[0], new String[] {});
			if (value != null) {
				if (value.getClass().isArray()) {
					result = Utils.arrayToString(value);
				} else {
					result = value.toString();
				}
			}

		} catch (final Exception e) {
			this.logger.error(e.getLocalizedMessage(), e);
		}
		if (result == null) {
			result = "No result returned.";
		}

		return result;
	}
	
	
	@Override
	protected void finalize() throws Throwable {
		this.connection.close();
		super.finalize();
	}

}
