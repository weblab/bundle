/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space SAS
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.bundle.utils;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.net.Authenticator;
import java.net.BindException;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.PasswordAuthentication;
import java.net.Socket;
import java.net.URL;
import java.util.Arrays;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.bundle.conf.WebLabBean;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 * Utility class
 *
 * @author asaval
 *
 */
public final class Utils {


	/**
	 * Utility class
	 */
	private Utils() {

	}


	/**
	 * Check if a port is available
	 *
	 * @param logger
	 *            The logger used inside
	 * @param host
	 *            The host to be used to open the socket
	 * @param port
	 *            a port
	 * @return true if the port is available, else false
	 */
	public static boolean isPortAvailable(final Log logger, final String host, final int port) {

		boolean available = true;
		try (final Socket socket = new Socket(host, port)) {
			logger.debug("Port " + port + " is listening.");
			// if we can open a sock; the port is already bind.
			available = false;
		} catch (@SuppressWarnings("unused") final BindException e) {
			// no problem
			logger.debug("Port " + port + " is free.");
		} catch (@SuppressWarnings("unused") final ConnectException e) {
			// no problem
			logger.debug("Port " + port + " is free.");
		} catch (final Exception e) {
			logger.error(e.getLocalizedMessage(), e);
			available = false;
		}
		return available;
	}


	/**
	 * Load default properties or those configured in weblab.properties file
	 * 
	 * @param logger
	 *            The logger used to write the system properties loaded
	 *
	 * @return a set of properties
	 */
	public static WebLabBean loadConfiguration(final Log logger) {

		// load configuration
		try (final FileSystemXmlApplicationContext factory = new FileSystemXmlApplicationContext("file:" + PathUtils.getWebLabHome("conf") + "configuration.xml")) {
			if (logger.isDebugEnabled()) {
				logger.debug("Loaded system properties: " + factory.getEnvironment().getSystemProperties());
			}
			return factory.getBean("WebLab", WebLabBean.class);
		}
	}


	/**
	 * Update a file
	 *
	 * @param logger
	 *            The logger used inside
	 * @param toReplace
	 *            The map of values to be replaced in the file
	 * @param file
	 *            The file to be updated
	 */
	public static void update(final Log logger, final Map<String, String> toReplace, final String file) {

		try {
			final File toUpdate = new File(file);
			// logger.info("Update "+file+" with map: "+toReplace);
			if (!toUpdate.exists()) {
				throw new FileNotFoundException("File " + toUpdate.getAbsolutePath() + " does not exist.");
			}
			String data = FileUtils.readFileToString(toUpdate);
			for (final String key : toReplace.keySet()) {
				data = data.replace(key, toReplace.get(key));
			}
			FileUtils.writeStringToFile(toUpdate, data);
		} catch (final IOException e) {
			logger.error(e.getLocalizedMessage(), e);
		}
	}


	/**
	 * Configure logger
	 *
	 * @return the logger
	 */
	public static Log getLogger() {

		return LogFactory.getLog("weblab.launcher");
	}


	/**
	 * If there second arg is not null and correspond to a server, return the server from enum
	 *
	 * @param logger
	 *            The logger used inside
	 * @param args
	 *            list of args
	 * @return the Server or null
	 */
	public static String getServer(final Log logger, final String[] args) {

		if ((args == null) || (args.length < 2) || (args[1] == null)) {
			logger.debug("Server selected : all");
			return null;
		}
		try {
			logger.debug("Try to find correct server for : \"" + args[1] + "\"");
			return args[1];
		} catch (final Exception e) {
			logger.warn(args[1] + " is not a valid value for command name. Use server's names defined in WebLab configuration file.", e);
		}
		return "";
	}


	/**
	 * print header for logger
	 *
	 * @param logger
	 *            The logger used inside
	 */
	public static void reinit(final Log logger) {

		final int size = 75;
		final BufferedImage image = new BufferedImage(size, 32, BufferedImage.TYPE_INT_RGB);
		final Graphics graph = image.getGraphics();
		graph.setFont(new Font(Font.SERIF, Font.BOLD | Font.ITALIC, 16));
		final Graphics2D graphics = (Graphics2D) graph;
		graphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		graphics.drawString("WebLab", 2, 24);

		final StringBuffer buffer = new StringBuffer();
		try {
			final File file = new File("text.png");
			file.deleteOnExit();
			ImageIO.write(image, "png", file);
			for (int y = 0; y < 32; y++) {
				final StringBuilder sbuffer = new StringBuilder();
				for (int x = 0; x < size; x++) {
					sbuffer.append(image.getRGB(x, y) == -16777216 ? " " : image.getRGB(x, y) == -1 ? (char) Math.round((Math.random() * 93) + 32) : (char) Math.round((Math.random() * 6) + 39));
				}
				if (sbuffer.toString().trim().isEmpty()) {
					continue;
				}
				buffer.append(sbuffer);
				buffer.append('\n');
			}
		} catch (@SuppressWarnings("unused") final IOException e) {
			buffer.append("I'd make a suggestion, but you wouldn't listen.");
		}

		logger.info(buffer.toString());
	}


	/**
	 * Reset a webdav collection
	 *
	 * @param logger
	 *            a logger
	 * @param webdavURL
	 *            a webdav url
	 * @param user
	 *            a username
	 * @param password
	 *            a password
	 * @return true if webdav collection has been reset.
	 */
	public static boolean resetWebDav(final Log logger, final String webdavURL, final String user, final String password) {

		boolean success = false;

		try {
			// delete webdav content
			Utils.httpRequest(logger, webdavURL, "DELETE", user, password, true);
			logger.info("WebDav contents at " + webdavURL + " deleted.");
			// create default collection
			Utils.httpRequest(logger, webdavURL, "MKCOL", user, password, true);
			logger.info("WebDav collection " + webdavURL + " ready for use.");
			success = true;
		} catch (final Exception e) {
			logger.error(e.getLocalizedMessage(), e);
		}

		return success;
	}


	/**
	 * Sends an HTTP Request with ANY method on an URL
	 *
	 * @param logger
	 *            a logger
	 * @param anurl
	 *            an url
	 * @param method
	 *            any method name
	 * @param user
	 *            a user
	 * @param password
	 *            a password
	 * @param verbose
	 *            if true it will log as INFO, else as FINEST
	 * @return the result of the request if any else the empty string.
	 * @throws Exception
	 *             If any wrong occurs
	 */
	public static String httpRequest(final Log logger, final String anurl, final String method, final String user, final String password, final boolean verbose) throws Exception {

		Authenticator.setDefault(new Authenticator() {


			@Override
			public PasswordAuthentication getPasswordAuthentication() {

				return new PasswordAuthentication(user, password.toCharArray());
			}
		});
		final URL url = new URL(anurl);
		final HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
		httpCon.setDoOutput(true);
		httpCon.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		final Field field = httpCon.getClass().getSuperclass().getDeclaredField("method");
		field.setAccessible(true);
		field.set(httpCon, method);
		httpCon.connect();

		try (final InputStream istream = httpCon.getInputStream(); final InputStream erstram = httpCon.getErrorStream()) {
			final String result = IOUtils.toString(istream);
			if (verbose) {
				logger.info(result);
			} else {
				logger.debug(result);
			}
			if (erstram != null) {
				logger.error(IOUtils.toString(erstram));
			}
			return result;
		}
	}


	/**
	 * Convert any array to a String
	 *
	 * @param array
	 *            an array
	 * @return a String describing the array
	 */
	public static String arrayToString(final Object array) {

		if (array instanceof int[]) {
			return Arrays.toString((int[]) array);
		} else if (array instanceof float[]) {
			return Arrays.toString((float[]) array);
		} else if (array instanceof char[]) {
			return Arrays.toString((char[]) array);
		} else if (array instanceof double[]) {
			return Arrays.toString((double[]) array);
		} else if (array instanceof boolean[]) {
			return Arrays.toString((boolean[]) array);
		} else if (array instanceof byte[]) {
			return Arrays.toString((byte[]) array);
		} else if (array instanceof long[]) {
			return Arrays.toString((long[]) array);
		} else if (array instanceof Object[]) {
			return Arrays.toString((Object[]) array);
		}
		return "Unsupported array : " + array.getClass();
	}


}