/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.bundle.utils.heritrix;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * This class is inspired from heritrix3-wrapper project.
 *
 * It represents the result of a JAXB unmarshaling of Heritrix answer when calling the engine page.
 *
 * @author ymombrun
 */
@XmlRootElement
public class Engine {


	@XmlElement(required = true)
	public String heritrixVersion;


	@XmlElement(required = true)
	public HeapReport heapReport;


	@XmlElement(required = true)
	public String jobsDir;


	@XmlElement(required = true)
	public String jobsDirUrl;


	@XmlElementWrapper(name = "availableActions")
	@XmlElement(name = "value", required = true)
	public List<String> availableActions;


	@XmlElementWrapper(name = "jobs")
	@XmlElement(name = "value", required = true)
	public List<Job> jobs;


	/**
	 * Default empty constructor
	 */
	public Engine() {
		super();
	}

}
