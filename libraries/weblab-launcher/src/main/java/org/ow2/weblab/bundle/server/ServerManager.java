/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.bundle.server;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.ow2.weblab.bundle.Constants;
import org.ow2.weblab.bundle.conf.WebLabBean;
import org.ow2.weblab.bundle.server.WebLabServer.State;
import org.ow2.weblab.bundle.utils.PathUtils;
import org.ow2.weblab.bundle.utils.ProcessUtils;
import org.ow2.weblab.bundle.utils.Utils;
import org.ow2.weblab.components.solr.utils.SolrUtils;
import org.springframework.util.CollectionUtils;

/**
 * Manage Server related commands
 *
 * @author asaval, lmartin, ymombrun
 */
public class ServerManager {


	private final Log logger;


	private final WebLabBean configuration;


	private final Map<String, String> webLabServersCommands = new HashMap<>();


	public ServerManager(final Log logger, final WebLabBean configuration) {

		this.logger = logger;
		this.configuration = configuration;

		// create a map of <name, server>
		if (configuration.getBus().isEnabled()) {
			this.webLabServersCommands.put(configuration.getBus().getName(), configuration.getBus().supportedCommands());
		}
		if (configuration.getPortal().isEnabled()) {
			this.webLabServersCommands.put(configuration.getPortal().getName(), configuration.getPortal().supportedCommands());
		}
		if (configuration.getActiveMQServer().isEnabled()) {
			this.webLabServersCommands.put(configuration.getActiveMQServer().getName(), configuration.getActiveMQServer().supportedCommands());
		}
		if (configuration.getApplicationServer().isEnabled()) {
			this.webLabServersCommands.put(configuration.getApplicationServer().getName(), configuration.getApplicationServer().supportedCommands());
		}
		for (final WebLabServer otherServer : configuration.getServers()) {
			if (otherServer.isEnabled()) {
				this.webLabServersCommands.put(otherServer.getName(), otherServer.supportedCommands());
			}
		}
	}


	private static final void displayStatus(final WebLabServer applicationServer, final Log logger) {

		if (!applicationServer.isEnabled()) {
			if (logger.isDebugEnabled()) {
				logger.debug(ServerManager.getServerDisplayedName(applicationServer) + " server is disabled");
			}
			return;
		}

		if (applicationServer.isServerFullyStarted(true)) {
			logger.info(ServerManager.getServerDisplayedName(applicationServer) + " server status: READY");
		} else {
			logger.info(ServerManager.getServerDisplayedName(applicationServer) + " server status: NOT STARTED.");
		}
		logger.info("");
	}


	private static final void displayStatus(final SolrServer solrServer, final Log logger) {

		if (!solrServer.isEnabled()) {
			if (logger.isDebugEnabled()) {
				logger.debug(ServerManager.getServerDisplayedName(solrServer) + " server is disable");
			}
			return;
		}

		if (solrServer.isServerFullyStarted(true)) {
			logger.info(ServerManager.getServerDisplayedName(solrServer) + " server status: READY");

			// is this solr node the "main" one (the one we used to create initial collections)
			final HashMap<String, HashMap<String, String>> initCollections = solrServer.getInitCollections();
			if (!CollectionUtils.isEmpty(initCollections)) {
				try (final CloudSolrClient solrClient = solrServer.getSolrClient()) {
					for (final String collection : initCollections.keySet()) {
						logger.info("\tCollection " + collection + " : READY");
						final Map<String, List<String>> replicas = SolrUtils.getReplicas(solrClient, collection);
						for (final Entry<String, List<String>> entry : replicas.entrySet()) {
							logger.info("\t\tNode " + entry.getKey());
							for (final String replica : entry.getValue()) {
								logger.info("\t\t\tReplica " + replica + " READY");
							}
						}
					}
				} catch (final IOException ioe) {
					logger.debug("An error occurred closing the solr client.", ioe);
				}
			} else {
				logger.info("No solr collection available");
			}
		} else {
			logger.info(ServerManager.getServerDisplayedName(solrServer) + " server status: NOT STARTED.");
		}
		logger.info("");
	}


	/**
	 * Check servers status
	 *
	 * @param server
	 *            The name of the server or <code>null</code> for all servers
	 */
	public void status(final String server) {

		final int timeout = 5000; // 5s is not too boring for the user. tssss... meatbags

		for (final WebLabServer otherServer : this.configuration.getServers()) {
			if ((server == null) || server.equals(otherServer.getName())) {
				if (otherServer instanceof SolrServer) {
					ServerManager.displayStatus((SolrServer) otherServer, this.logger);
				} else {
					ServerManager.displayStatus(otherServer, this.logger);
				}
			}
		}

		if ((server == null) || server.equals(this.configuration.getApplicationServer().getName())) {
			final TomcatServer tomcat = this.configuration.getApplicationServer();
			tomcat.setTimeout(timeout);
			ServerManager.displayStatus(tomcat, this.logger);
		}

		if ((server == null) || server.equals(this.configuration.getPortal().getName())) {
			final TomcatServer portal = this.configuration.getPortal();
			portal.setTimeout(timeout);
			ServerManager.displayStatus(portal, this.logger);
		}

		if ((server == null) || server.equals(this.configuration.getBus().getName())) {
			final Bus bus = this.configuration.getBus();
			bus.setTimeout(timeout);
			ServerManager.displayStatus(bus, this.logger);
		}

		if ((server == null) || server.equals(this.configuration.getActiveMQServer().getName())) {
			final ActiveMQServer activeMQServer = this.configuration.getActiveMQServer();
			activeMQServer.setTimeout(timeout);
			ServerManager.displayStatus(activeMQServer, this.logger);
		}

	}


	/**
	 * @param server
	 *            The server to stop
	 * @param force
	 *            Whether or not to kill the server if stopping it fails
	 * @param logger
	 *            The logger to be used for messages
	 */
	public static final void stop(final WebLabServer server, final boolean force, final Log logger) {

		if (!server.isEnabled()) {
			if (logger.isDebugEnabled()) {
				logger.debug(ServerManager.getServerDisplayedName(server) + " server is disable");
			}
			return;
		}

		final State currentServerState = server.status();
		if (currentServerState == State.STARTING) {
			logger.error(ServerManager.getServerDisplayedName(server) + " server is starting. Please try again later.");
		} else if (currentServerState != State.STOPPED) {
			server.stop();
			ProcessUtils.dieAfterTimeout(logger, server, force);

			if (server.getCleanablePathsAfterStop() != null) {
				for (final String pathToClean : server.getCleanablePathsAfterStop()) {
					final File fileOrFolderToClean = new File(pathToClean);
					if (fileOrFolderToClean.exists() && fileOrFolderToClean.isDirectory()) {
						final long size = FileUtils.sizeOf(fileOrFolderToClean);
						if (size > 0) {
							logger.info("Cleaning directory '" + fileOrFolderToClean.getAbsolutePath() + "'. Size before cleaning: " + FileUtils.byteCountToDisplaySize(size));
							try {
								FileUtils.cleanDirectory(fileOrFolderToClean);
							} catch (final IOException ioe) {
								logger.debug("An error occurred cleaning directory " + fileOrFolderToClean.getAbsolutePath(), ioe);
							}
							logger.info("Directory '" + fileOrFolderToClean.getAbsolutePath() + "' size after cleaning: " + FileUtils.byteCountToDisplaySize(FileUtils.sizeOf(fileOrFolderToClean)));
						}

					} else if (fileOrFolderToClean.exists() && fileOrFolderToClean.isFile()) {
						final boolean done = FileUtils.deleteQuietly(fileOrFolderToClean);
						if (!done) {
							logger.debug("Unable to remove file " + fileOrFolderToClean.getAbsolutePath());
						}
					}
				}
			}
			logger.info("");
		}
	}


	/**
	 * Stop servers
	 *
	 * @param server
	 *            the server to stop (may be none, one or all)
	 * @param force
	 *            if true force the stop without asking the user else respectfully ask the user
	 */
	public void stop(final String server, final boolean force) {

		if (this.logger.isDebugEnabled()) {
			this.logger.debug("Stopping server: " + server + " (force=" + force + ")");
		}

		// Stops Liferay
		if ((server == null) || server.equals(this.configuration.getPortal().getName())) {
			final TomcatServer portal = this.configuration.getPortal();
			ServerManager.stop(portal, force, this.logger);
		}

		// Stops Bus
		if ((server == null) || server.equals(this.configuration.getBus().getName())) {
			final Bus bus = this.configuration.getBus();
			ServerManager.stop(bus, force, this.logger);
		}

		// Stops activeMQ
		if ((server == null) || server.equals(this.configuration.getActiveMQServer().getName())) {
			final ActiveMQServer activeMQServer = this.configuration.getActiveMQServer();
			ServerManager.stop(activeMQServer, force, this.logger);
		}

		// Stops tomcat
		if ((server == null) || server.equals(this.configuration.getApplicationServer().getName())) {
			final TomcatServer tomcat = this.configuration.getApplicationServer();
			ServerManager.stop(tomcat, force, this.logger);
		}

		for (final WebLabServer otherServer : this.configuration.getServers()) {
			if ((server == null) || server.equals(otherServer.getName())) {
				ServerManager.stop(otherServer, force, this.logger);
			}
		}

		if (server == null) {
			this.logger.info(this.configuration.getName() + " is stopping ... ");
		}

	}


	/**
	 * Start server if it is enable.
	 *
	 * @param server
	 *            The server to start
	 * @param logger
	 *            The logger used for the messages
	 * @param waitForStart
	 *            Whether or not to wait for the end of server startup
	 * @return Return process.
	 */
	public static final Process start(final WebLabServer server, final Log logger, final boolean waitForStart) {

		if (server == null) {
			return null;
		}

		if (server.isEnabled()) {
			final Process p = server.start();
			Thread.yield();
			State state = server.status();

			if (waitForStart) {

				int tries = 5;
				while ((state != State.STARTED) && (tries > 0)) {
					tries--;
					if (logger.isDebugEnabled()) {
						logger.debug(ServerManager.getServerDisplayedName(server) + " not started yet. Wait a few seconds and try to get status again.");
					}
					Thread.yield();
					try {
						Thread.sleep(10000);
					} catch (final InterruptedException ie) {
						if (logger.isDebugEnabled()) {
							logger.debug(ServerManager.getServerDisplayedName(server) + " produced an error when waiting for its start.", ie);
						}
					}
					Thread.yield();
					state = server.status();
				}

				if (state != State.STARTED) {
					if (server.isMandatory()) {
						logger.error("Server " + server.getName() + " is not started but " + state.toString() + ". Since it is a mandatory server. Stop here!");
						System.exit(666);
						return null;
					}
					logger.warn("Server " + server.getName() + " is not started but " + state.toString() + ". Since it is not a mandatory server. Continue.");
				} else if (logger.isDebugEnabled()) {
					logger.debug(ServerManager.getServerDisplayedName(server) + " server is started");
				}
			}

			return p;
		} else if (logger.isDebugEnabled()) {
			logger.debug(ServerManager.getServerDisplayedName(server) + " is not enabled.");
		}
		return null;
	}


	/**
	 * Starts servers
	 *
	 * @param server
	 *            the server to start of null to start them all
	 */
	public void start(final String server) {

		for (final WebLabServer otherServer : this.configuration.getServers()) {
			if ((server == null) || server.equals(otherServer.getName())) {
				ServerManager.start(otherServer, this.logger, true);
				this.logger.info("");
			}
		}

		// Starts Tomcat
		if ((server == null) || server.equals(this.configuration.getApplicationServer().getName())) {
			// configure Services spring beans only if we launch tomcat
			this.configureServicesFiles();

			final TomcatServer tomcat = this.configuration.getApplicationServer();
			final Process process = ServerManager.start(tomcat, this.logger, false); // Do wait inside the script, use the become ready of die
			if (process != null) {
				ProcessUtils.okOrFail(this.logger, process, "Apache Tomcat failed to start, see " + tomcat.getSubDir("logs") + "catalina.out for more details. WebLab start abort.",
						Constants.TOMCAT_FAILED);

				if (tomcat.becomeReadyOrDieTrying()) {
					this.logger.info("Apache Tomcat is started");
				} else {
					this.logger.error("Tomcat could not completely start (server and all needed webapps) in less than " + tomcat.getTimeout() + "ms. Please check previous error if any or see "
							+ tomcat.getSubDir("logs") + "catalina.out for more details or configure launcher in debug mode.");
					System.exit(Constants.TOMCAT_FAILED);
				}
			}
			this.logger.info("");
		}

		// Starts ActiveMQ server
		if ((server == null) || server.equals(this.configuration.getActiveMQServer().getName())) {
			ServerManager.start(this.configuration.getActiveMQServer(), this.logger, true);
			this.logger.info("");
		}

		// Starts Bus
		if ((server == null) || server.equals(this.configuration.getBus().getName())) {
			ServerManager.start(this.configuration.getBus(), this.logger, true);
			this.logger.info("");
		}

		// Starts Liferay
		if ((server == null) || server.equals(this.configuration.getPortal().getName())) {
			// configure portlet spring beans only if we launch liferay
			this.configurePortletsFiles();

			final TomcatServer liferay = this.configuration.getPortal();
			if (liferay.isEnabled()) {
				final Process process = ServerManager.start(liferay, this.logger, false); // Do wait inside the script, use the become ready of die
				ProcessUtils.okOrFail(this.logger, process, "Liferay failed to start, see " + liferay.getSubDir("logs") + "catalina.out for more details. WebLab start abort.",
						Constants.LIFERAY_FAILED);

				if (liferay.becomeReadyOrDieTrying()) {
					this.logger.info("Liferay is started");
				} else {
					this.logger.error("Liferay could not completely start (server and all needed webapps). Please check previous error if any or see " + liferay.getSubDir("logs")
							+ "catalina.out for more details.");
				}
			}
			this.logger.info("");
		}

	}


	/**
	 * Execute a remote method on a TomcatServer or on ActiveMQ and display results
	 *
	 * @param server
	 *            The server on which the request should be sent
	 * @param namedParameter
	 *            The name of the webapp to receive the request
	 * @param args
	 *            The args to send (in fact only args[2] is sent)
	 */
	public void rmi(final String server, final String namedParameter, final String[] args) {

		// retrieve webapp name and command
		if ((args == null) || (args.length < 3) || (args[2] == null)) {
			this.logger.warn("Command " + Arrays.asList(args) + " does not match rmi syntax : \"server webapp|queue start|stop|purge\"");
			return;
		}

		final String command = args[2];

		if ((server != null) && server.equals(this.configuration.getPortal().getName())) {
			final TomcatServer tomcat = this.configuration.getPortal();
			tomcat.executeOnWebApp(namedParameter, command);
		} else if ((server != null) && server.equals(this.configuration.getApplicationServer().getName())) {
			final TomcatServer tomcat = this.configuration.getApplicationServer();
			tomcat.executeOnWebApp(namedParameter, command);
		} else if ((server != null) && server.equals(this.configuration.getActiveMQServer().getName())) {
			final ActiveMQServer activemq = this.configuration.getActiveMQServer();
			if (!"purge".equals(command)) {
				this.logger.warn("Command " + command + " is not supported on " + server + ".");
			} else {
				activemq.purge(namedParameter);
			}
		} else if ((server != null) && server.equals(this.configuration.getBus().getName())) {
			final Bus bus = this.configuration.getBus();
			if ("start".equals(command)) {
				bus.startChain(namedParameter);
			} else if ("stop".equals(command)) {
				bus.stopChain(namedParameter);
			} else if ("status".equals(command)) {
				this.logger.info(bus.statusChain(namedParameter));
			} else if ("restart".equals(command)) {
				bus.stopChain(namedParameter);
				bus.startChain(namedParameter);
			} else {
				this.logger.warn("Action : \"" + command + "\" not supported on bus.");
			}

		} else {
			this.logger.warn(server + " does not support remote execution.");
		}
	}


	/**
	 * Replace cxf files for services
	 */
	private void configureServicesFiles() {

		// check if services configuration is enabled
		if (!this.configuration.isServicesReconfigure()) {
			this.logger.warn("Services configuration is not enabled.");
			return;
		}

		// check if files are configured
		// List files to replace
		final String servicesConfPath = PathUtils.createPath(PathUtils.getWebLabHome("conf"), "services");
		final File servicesConfDir = new File(servicesConfPath);
		if (!servicesConfDir.isDirectory()) {
			this.logger.error(servicesConfDir.getAbsolutePath() + " is not a directory. Aborting services reconfiguration.");
			return;
		}

		// select matching files 'serviceName.cxfFile.xml'
		final List<File> serviceConfs = new LinkedList<>();
		for (final File file : servicesConfDir.listFiles()) {
			final String name = file.getName();
			if (file.isFile() && name.matches(".+\\..+\\.xml")) {
				if (this.logger.isDebugEnabled()) {
					this.logger.debug("Matching file " + file.getAbsolutePath() + " to replace.");
				}
				serviceConfs.add(file);
			} else {
				this.logger.warn("Discarding file " + file.getAbsolutePath() + " because " + name + " does not match 'serviceName.cxfFile.xml' pattern.");
			}
		}

		// retrieve sample file
		final File cxfSample = new File(PathUtils.createPath(PathUtils.getWebLabHome("conf"), "samples") + "cxf-bean.xml.sample");

		final Map<String, String> replacement = new HashMap<>();

		final TomcatServer tomcat = this.configuration.getApplicationServer();

		// Replace services files by overwriting configuration
		for (final File file : serviceConfs) {
			// parse file
			final String[] name = file.getName().split("\\.");
			final String service = name[0];
			final String cxfFileName = name[1] + "." + name[2];
			// config file from service:
			final File serviceConfigFile = tomcat.getServiceConfigurationFile(service, cxfFileName);
			replacement.clear();
			try {
				// overwrite service conf
				FileUtils.copyFile(cxfSample, serviceConfigFile);
				if (this.logger.isDebugEnabled()) {
					this.logger.debug("Updating configuration for service " + service + " with cxf-bean from " + file.getAbsolutePath());
				}
				replacement.put("$SERVICECONFIGURL$", file.toURI().toURL().toString());
				Utils.update(this.logger, replacement, serviceConfigFile.getAbsolutePath());
			} catch (final IOException ioe) {
				this.logger.error(ioe.getMessage(), ioe);
			}
		}
		if (!serviceConfs.isEmpty()) {
			this.logger.info("Updated services configuration: " + serviceConfs);
		}
	}


	/**
	 * Replace cxf files for portlets
	 */
	private void configurePortletsFiles() {

		// TODO This empty method is not defined yet
	}


	public Map<String, String> getServersCommands() {

		return this.webLabServersCommands;
	}


	/**
	 *
	 * @param server
	 * @return
	 */
	private static String getServerDisplayedName(final WebLabServer server) {

		if ((server == null) || (server.getName() == null)) {
			return null;
		}

		return server.getName().toUpperCase();
	}

}