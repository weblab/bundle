/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.bundle.conf;

import java.util.LinkedList;
import java.util.List;

import org.ow2.weblab.bundle.server.ActiveMQServer;
import org.ow2.weblab.bundle.server.Bus;
import org.ow2.weblab.bundle.server.TomcatServer;
import org.ow2.weblab.bundle.server.WebLabServer;

/**
 * WebLab configuration params
 *
 * @author asaval
 *
 */
public class WebLabBean {


	private String name = "WebLab";


	// TODO remove ?
	@Deprecated
	private String debug = "info";


	// TODO remove ?
	@Deprecated
	private String home = "info";


	// TODO remove ?
	@Deprecated
	private boolean jmxEnabled = true;


	private float classVersion = 52.0f;


	private String contentFolder = "data/content";


	private boolean remoteAccess = false;


	private String remoteLogin = "weblab";


	private String remotePassword = "weblab";


	private String repository = "data/repository";


	private boolean servicesReconfigure = true;


	// TODO remove ?
	@Deprecated
	private String split = "";


	// TODO remove ?
	@Deprecated
	private long timeout = 300000;


	// TODO remove ?
	@Deprecated
	private String toindex = "data/toIndex";


	// TODO remove ?
	@Deprecated
	private String warcs = "data/warcs";


	private String webdavPassword = "demo";


	private String webdavUrl = null;


	private String webdavUser = "demo@weblab-project.org";


	private String resetScript;


	// TODO remove ?
	@Deprecated
	private String chainManagerService;


	private List<WebLabServer> servers = new LinkedList<>();


	private TomcatServer portal;


	private Bus bus;


	private ActiveMQServer activeMQServer;


	private TomcatServer applicationServer;


	@Deprecated
	public String getDebug() {
		return this.debug;
	}


	@Deprecated
	public void setDebug(final String debug) {
		this.debug = debug;
	}


	@Deprecated
	public String getHome() {
		return this.home;
	}


	@Deprecated
	public void setHome(final String home) {
		this.home = home;
	}


	public float getClassVersion() {
		return this.classVersion;
	}


	public void setClassVersion(final float classVersion) {
		this.classVersion = classVersion;
	}


	@Deprecated
	public boolean isJmxEnabled() {
		return this.jmxEnabled;
	}


	@Deprecated
	public void setJmxEnabled(final boolean jmxEnabled) {
		this.jmxEnabled = jmxEnabled;
	}


	public boolean isRemoteAccess() {
		return this.remoteAccess;
	}


	public void setRemoteAccess(final boolean remoteAccess) {
		this.remoteAccess = remoteAccess;
	}


	public String getRemoteLogin() {
		return this.remoteLogin;
	}


	public void setRemoteLogin(final String remoteLogin) {
		this.remoteLogin = remoteLogin;
	}


	public String getRemotePassword() {
		return this.remotePassword;
	}


	public void setRemotePassword(final String remotePassword) {
		this.remotePassword = remotePassword;
	}


	public String getRepository() {
		return this.repository;
	}


	public void setRepository(final String repository) {
		this.repository = repository;
	}


	public boolean isServicesReconfigure() {
		return this.servicesReconfigure;
	}


	public void setServicesReconfigure(final boolean servicesReconfigure) {
		this.servicesReconfigure = servicesReconfigure;
	}


	@Deprecated
	public String getSplit() {
		return this.split;
	}


	@Deprecated
	public void setSplit(final String split) {
		this.split = split;
	}


	@Deprecated
	public long getTimeout() {
		return this.timeout;
	}


	@Deprecated
	public void setTimeout(final long timeout) {
		this.timeout = timeout;
	}


	@Deprecated
	public String getToindex() {
		return this.toindex;
	}


	@Deprecated
	public void setToindex(final String toindex) {
		this.toindex = toindex;
	}


	@Deprecated
	public String getWarcs() {
		return this.warcs;
	}


	@Deprecated
	public void setWarcs(final String warcs) {
		this.warcs = warcs;
	}


	public String getWebdavPassword() {
		return this.webdavPassword;
	}


	public void setWebdavPassword(final String webdavPassword) {
		this.webdavPassword = webdavPassword;
	}


	public String getWebdavUrl() {
		return this.webdavUrl;
	}


	public void setWebdavUrl(final String webdavUrl) {
		this.webdavUrl = webdavUrl;
	}


	public String getWebdavUser() {
		return this.webdavUser;
	}


	public void setWebdavUser(final String webdavUser) {
		this.webdavUser = webdavUser;
	}


	public List<WebLabServer> getServers() {
		return this.servers;
	}


	public void setServers(final List<WebLabServer> servers) {
		this.servers = servers;
	}


	public TomcatServer getPortal() {
		return this.portal;
	}


	public void setPortal(final TomcatServer portal) {
		this.portal = portal;
	}


	public Bus getBus() {
		if (this.bus == null) {
			this.bus = new Bus.DisabledBus();
		}
		return this.bus;
	}


	public void setBus(final Bus bus) {
		this.bus = bus;
	}


	public TomcatServer getApplicationServer() {
		return this.applicationServer;
	}


	public void setApplicationServer(final TomcatServer applicationServer) {
		this.applicationServer = applicationServer;
	}


	@Deprecated
	public String getChainManagerService() {
		return this.chainManagerService;
	}


	@Deprecated
	public void setChainManagerService(final String chainManagerService) {
		this.chainManagerService = chainManagerService;
	}


	public String getName() {
		return this.name;
	}


	public void setName(final String name) {
		this.name = name;
	}


	/**
	 * @return the resetScript
	 */
	public String getResetScript() {
		return this.resetScript;
	}


	/**
	 * @param resetScript
	 *            the resetScript to set
	 */
	public void setResetScript(final String resetScript) {
		this.resetScript = resetScript;
	}


	/**
	 * @return the activeMQServer
	 */
	public ActiveMQServer getActiveMQServer() {
		return this.activeMQServer;
	}


	/**
	 * @param activeMQServer
	 *            the activeMQServer to set
	 */
	public void setActiveMQServer(final ActiveMQServer activeMQServer) {
		this.activeMQServer = activeMQServer;
	}



	/**
	 * @return the contentFolder
	 */
	public String getContentFolder() {
		return this.contentFolder;
	}




	/**
	 * @param contentFolder
	 *            the contentFolder to set
	 */
	public void setContentFolder(final String contentFolder) {
		this.contentFolder = contentFolder;
	}
}
