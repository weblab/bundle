/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.bundle;


/**
 * WebLab Launcher Constants
 *
 * @author asaval
 *
 */
public final class Constants {


	public static final int TOMCAT_FAILED = 1;


	public static final int LIFERAY_FAILED = 2;


	public static final int BUS_FAILED = 3;


	public static final int DEPLOY_FAILED = 4;


	public static final int CONNECTION_FAILED = 5;


	public static final int COMMAND_FAILED = 6;


	public static final int FUSEKI_FAILED = 7;


	public static final int OTHER_SERVER_FAILED = 8;


	public static final int ACTIVEMQ_FAILED = 9;


	public static final int SOLR_FAILED = 10;

}
