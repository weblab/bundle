/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space SAS
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.bundle.server;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

import javax.management.remote.JMXConnector;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.ow2.weblab.bundle.utils.ProcessUtils;
import org.ow2.weblab.bundle.utils.Utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Implementation of WebLab server to manage a Fuseki triple store server.
 *
 * @author lamartin
 */
public class Fuseki extends WebLabServer {


	/**
	 * The default constructor
	 */
	public Fuseki() {
		// Nothing to do
	}


	@Override
	public String getProcessIdentificationClue() {
		return this.getHome();
	}


	@Override
	public boolean isServerFullyStarted(final boolean showDetails) {
		if (!this.isEnabled()) {
			this.logger.info("Can not check if " + this.getName() + " is started since control is disabled.");
			return true;
		}
		if (!Utils.isPortAvailable(this.logger, this.getHost(), this.getPort())) {
			this.logger.info(this.getName() + " is started.");
			if (showDetails) {
				try (final CloseableHttpClient client = HttpClients.custom().build();) {
					final HttpGet httpget = new HttpGet(new URL("http", this.getHost(), this.getPort(), "/$/datasets").toURI());
					final String result = client.execute(httpget, new BasicResponseHandler());
					this.logger.debug(result);
					final JsonObject json = new JsonParser().parse(result).getAsJsonObject();
					final JsonArray datasets = json.get("datasets").getAsJsonArray();
					for (final JsonElement datasetElm : datasets) {
						this.logger.info("\tDataset: " + datasetElm.getAsJsonObject().get("ds.name").getAsString() + ".");
					}
				} catch (final IOException ioe) {
					this.logger.error("Unable to talk with Fuseki.", ioe);
				} catch (final URISyntaxException urise) {
					this.logger.error("Unable to talk with Fuseki.", urise);
				} catch (final Exception e) {
					this.logger.error("Unable to parse result from Fuseki.", e);
				}
			}
			return true;
		}
		return false;
	}



	/**
	 * Execute a command
	 *
	 * @param command
	 *            The string command to execute (start, stop or status)
	 * @return The process description
	 */
	public Process execute(final String command) {
		if (!this.isEnabled()) {
			this.logger.info(this.getName() + " discarded command " + command + " since control is disabled.");
			return ProcessUtils.dummyProccess();
		}
		if ("start".equals(command)) {
			return this.start();
		} else if ("stop".equals(command)) {
			this.stop();
			return ProcessUtils.dummyProccess();
		} else if ("status".equals(command)) {
			this.logger.info(this.getName() + " getting status...");
		} else {
			this.logger.warn("Command " + command + " is not handled by " + this.getName());
			return null;
		}
		return this.execute(this.getEnv(), command);
	}


	@Override
	public JMXConnector getJMXConnector() {
		return null;
	}


	@Override
	public Process start() {
		// check status
		if (this.status() != State.STOPPED) {
			this.logger.error(this.getName() + " is not stopped! Aborting start...");
			return null;
		}

		// check pid file
		if (ProcessUtils.checkPidFile(this.logger, this, false)) {
			// server already started; we abort.
			this.logger.error(this.getName() + " already started! Aborting...");
			return null;
		}

		this.logger.info(this.getName() + " is starting ...");
		return this.execute(this.getEnv(), "start");
	}


	@Override
	public void stop() {
		this.logger.info(this.getName() + " is stopping ...");
		this.execute(this.getEnv(), "stop");
	}

}