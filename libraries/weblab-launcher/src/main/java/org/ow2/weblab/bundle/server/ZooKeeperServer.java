/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space SAS
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.bundle.server;

import javax.management.remote.JMXConnector;

import org.ow2.weblab.bundle.utils.PathUtils;
import org.ow2.weblab.bundle.utils.Utils;

/**
 * Zookeeper server used by solr cloud instances
 * 
 * @author lmartin
 */
public class ZooKeeperServer extends AbstractApplicationServer {


	private String confFile;


	/**
	 * The default constructor
	 */
	public ZooKeeperServer() {
		// Nothing to do
	}


	@Override
	protected String[] getStartScript() {
		// ZOOKEEPER-1122, no start/stop command on windows...
		if (System.getProperty("os.name").contains("Windows")) {
			return new String[] { PathUtils.createPath(this.getHome(), "bin") + "zkServer.cmd"};
		}
		return new String[] { PathUtils.createPath(this.getHome(), "bin") + "zkServer.sh", WebLabServer.START, this.confFile };
	}


	@Override
	protected String[] getStopScript() {
		// ZOOKEEPER-1122, no start/stop command on windows... and no stop at all in fact!
		if (System.getProperty("os.name").contains("Windows")) {
			return null;
		}
		return new String[] { PathUtils.createPath(this.getHome(), "bin") + "zkServer.sh", WebLabServer.STOP, this.confFile };
	}


	@Override
	public JMXConnector getJMXConnector() {
		return null;
	}


	@Override
	public String getProcessIdentificationClue() {
		return this.confFile;
	}


	@Override
	public boolean isServerFullyStarted(final boolean showDetails) {
		return !Utils.isPortAvailable(this.logger, this.getHost(), this.getPort());
	}


	/**
	 * @return the confFile
	 */
	public String getConfFile() {
		return this.confFile;
	}


	/**
	 * @param confFile
	 *            the confFile to set
	 */
	public void setConfFile(final String confFile) {
		this.confFile = confFile;
	}

}
