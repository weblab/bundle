/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.bundle.server;

import javax.management.remote.JMXConnector;

import org.ow2.weblab.bundle.utils.ProcessUtils;

/**
 * ESB management class
 * @author asaval
 *
 */
public abstract class Bus extends AbstractApplicationServer {

	/**
	 * execute a command line on the bus
	 * @param command command to execute
	 * @return the process corresponding to the command launched
	 */
	public abstract Process execute(String command);

	public abstract boolean startChain(String chain);

	public abstract boolean stopChain(String chain);

	public abstract void listChains();

	public abstract String statusChain(String chain);

	public static class DisabledBus extends Bus {

		public DisabledBus(){
			super.setName("Disabled Bus");
			super.setEnabled(false);
			super.setMandatory(false);
		}

		@Override
		public Process execute(final String command) {
			return ProcessUtils.dummyProccess();
		}

		@Override
		public String getProcessIdentificationClue() {
			return "Disabled Bus";
		}

		@Override
		public Process start() {
			return ProcessUtils.dummyProccess();
		}

		@Override
		public void stop() {/* Not implemented */}

		@Override
		public boolean isServerFullyStarted(final boolean showDetails) {
			return true;
		}

		@Override
		public JMXConnector getJMXConnector() {
			return null;
		}

		@Override
		public boolean startChain(final String chain) {
			return false;
		}

		@Override
		public boolean stopChain(final String chain) {
			return false;
		}

		@Override
		public void listChains() {
			// Doing nothing
		}

		@Override
		public String statusChain(final String chain) {
			return null;
		}

		@Override
		protected String[] getStopScript() {
			return null;
		}

		@Override
		protected String[] getStartScript() {
			return null;
		}

	}

}
