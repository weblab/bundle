/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space SAS
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.bundle.server;

import java.io.File;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import javax.management.JMX;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectInstance;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import org.apache.camel.api.management.mbean.ManagedSuspendableRouteMBean;
import org.apache.commons.io.FileUtils;
import org.ow2.weblab.bundle.utils.PathUtils;
import org.ow2.weblab.bundle.utils.ProcessUtils;
import org.ow2.weblab.bundle.utils.Utils;

public class Karaf extends Bus {


	private String stopScript;


	private String jmxUser = "karaf";


	private String jmxPassword = "karaf";


	private String jmxHost = "localhost";


	private String jmxName = "karaf-weblab";


	public Karaf() {

		// Nothing to do
	}


	public String getJmxUser() {

		return this.jmxUser;
	}


	public void setJmxUser(final String jmxUser) {

		this.jmxUser = jmxUser;
	}


	public String getJmxPassword() {

		return this.jmxPassword;
	}


	public void setJmxPassword(final String jmxPassword) {

		this.jmxPassword = jmxPassword;
	}


	public String getJmxHost() {

		return this.jmxHost;
	}


	public void setJmxHost(final String jmxHost) {

		this.jmxHost = jmxHost;
	}


	@Override
	public Process execute(final String command) {

		if (!this.isEnabled()) {
			this.logger.info(this.getName() + " discarded command " + command + " since control is disabled.");
			return ProcessUtils.dummyProccess();
		}
		if (WebLabServer.START.equals(command)) {
			// start
			return this.start();
		} else if (WebLabServer.STOP.equals(command)) {
			// stop
			this.logger.info(this.getName() + " is stopping ...");
			this.stop();
		} else if (WebLabServer.STATUS.equals(command)) {
			return this.detailedStatus();
		}
		return null;
	}


	/**
	 * override web console port from http://servicemix.apache.org/docs/4.5.x/users-guide/web-console.html in config file etc/org.ops4j.pax.web.cfg with
	 * org.osgi.service.http.port={port}
	 */
	@Override
	protected void checkConfiguration() {

		final String configweb = PathUtils.createPath(this.getHome(), "etc") + "org.ops4j.pax.web.cfg";
		try {
			// check if the file exists
			final File configFile = new File(configweb);
			if (!configFile.exists()) {
				if (this.logger.isDebugEnabled()) {
					this.logger.debug("Updating " + this.getName() + " web console listening port.");
				}
				FileUtils.writeStringToFile(new File(configweb), "org.osgi.service.http.port=" + this.getPort());
			} else {
				// TODO: compare the configured port from local spring config to servicemix configured port and alter if they do not match
				this.logger.info("Web console listening port configuration file " + configFile.getAbsolutePath() + " already exists for " + this.getName());
			}

		} catch (final IOException ioe) {
			this.logger.error(this.getName() + " could not set web console port to " + this.getPort() + " beacuse of the following error: ", ioe);
		}
	}


	@Override
	public String getProcessIdentificationClue() {

		return this.getHome();
	}


	@Override
	public boolean isServerFullyStarted(final boolean showDetails) {

		if (!this.isEnabled()) {
			this.logger.info("Can not check if " + this.getName() + " is started since control is disabled.");
			return true;
		}
		this.logger.info("Checking " + this.getName() + " status ... ");
		this.detailedStatus();
		boolean running = !Utils.isPortAvailable(this.logger, this.getHost(), this.getJmxPort());
		if (showDetails && running) {
			this.listChains();
		}
		return running;
	}


	private Process detailedStatus() {

		final String ext = System.getProperty("os.name").contains("Windows") ? ".bat" : "";
		return ProcessUtils.runProcess(this.logger, this.getBinDirectory(), this.getEnv(), PathUtils.createPath(this.getHome(), "bin") + "status" + ext);
	}


	@Override
	public JMXConnector getJMXConnector() {

		return ProcessUtils.createJMXclient(this.jmxHost, this.getJmxPort(), this.jmxUser, this.jmxPassword, this.jmxName, 2000, false, this.logger);
	}


	@Override
	public String[] getStartScript() {

		String script = this.getScript();
		if (script == null) {
			final String ext = System.getProperty("os.name").contains("Windows") ? ".bat" : "";
			script = PathUtils.createPath(this.getHome(), "bin") + "start" + ext;
		}
		return new String[] { script };
	}


	@Override
	public String[] getStopScript() {

		String script = this.stopScript;
		if (script == null) {
			final String ext = System.getProperty("os.name").contains("Windows") ? ".bat" : "";
			script = PathUtils.createPath(this.getHome(), "bin") + "stop" + ext;
		}
		return new String[] { script };
	}


	public void setStopScript(final String stopScript) {

		this.stopScript = stopScript;
	}


	/**
	 * Retrieve MbeanServerConnection using jmx params
	 *
	 * @return a MbeanServerConnection to karaf
	 * @throws IOException
	 *             If an error occurs openning the connection
	 */
	protected JMXConnector connect() throws IOException {

		final JMXServiceURL url = new JMXServiceURL("service:jmx:rmi:///jndi/rmi://" + this.jmxHost + ":" + this.getJmxPort() + "/" + this.jmxName);
		final Hashtable<String, Object> env = new Hashtable<>();
		env.put("java.naming.factory.initial", "com.sun.jndi.rmi.registry.RegistryContextFactory");
		env.put(JMXConnector.CREDENTIALS, new String[] { this.jmxUser, this.jmxPassword });

		return JMXConnectorFactory.connect(url, env);
	}


	@Override
	public void listChains() {

		try (final JMXConnector jmx = this.connect()) {
			final MBeanServerConnection conns = jmx.getMBeanServerConnection();
			final Set<ObjectInstance> objects = conns.queryMBeans(new ObjectName("org.apache.camel:context=*,type=routes,name=*"), null);

			final Map<String, String> sortedmap = new TreeMap<>();

			for (final ObjectInstance oi : objects) {
				final ManagedSuspendableRouteMBean msr = JMX.newMBeanProxy(conns, oi.getObjectName(), ManagedSuspendableRouteMBean.class);
				sortedmap.put(msr.getRouteId(), msr.getState());
			}

			this.logger.info("List Camel chains available on " + this.getName());
			for (final Entry<String, String> entry : sortedmap.entrySet()) {
				this.logger.info("\t" + entry.getKey() + " : " + entry.getValue().toUpperCase());
			}

		} catch (final Exception exception) {
			this.logger.error(exception.getMessage(), exception);
		}
	}


	private static ManagedSuspendableRouteMBean getRoute(final MBeanServerConnection conns, final String routeId) throws IOException, MalformedObjectNameException, NullPointerException {

		ManagedSuspendableRouteMBean route = null;

		final Set<ObjectInstance> objects = conns.queryMBeans(new ObjectName("org.apache.camel:context=*,type=routes,name=\"" + routeId + "\""), null);
		if (!objects.isEmpty()) {
			final ObjectInstance instance = objects.iterator().next();
			if (instance != null) {
				route = JMX.newMBeanProxy(conns, instance.getObjectName(), ManagedSuspendableRouteMBean.class);
			}
		}
		return route;
	}


	@Override
	public boolean startChain(final String routeId) {

		try (final JMXConnector jmx = this.connect()) {
			final MBeanServerConnection conns = jmx.getMBeanServerConnection();
			final ManagedSuspendableRouteMBean route = Karaf.getRoute(conns, routeId);
			if (route != null) {
				route.start();
				return true;
			}

		} catch (final Exception exception) {
			this.logger.error(exception.getMessage(), exception);
		}

		return false;
	}


	@Override
	public boolean stopChain(final String routeId) {

		try (final JMXConnector jmx = this.connect()) {
			final MBeanServerConnection conns = jmx.getMBeanServerConnection();
			final ManagedSuspendableRouteMBean route = Karaf.getRoute(conns, routeId);
			if (route != null) {
				route.start();
				return true;
			}

		} catch (final Exception exception) {
			this.logger.error(exception.getMessage(), exception);
		}

		return false;
	}


	@Override
	public String statusChain(final String routeId) {

		try (final JMXConnector jmx = this.connect()) {
			final MBeanServerConnection conns = jmx.getMBeanServerConnection();
			final ManagedSuspendableRouteMBean route = Karaf.getRoute(conns, routeId);
			if (route != null) {
				return route.getState() + "\n" + route.dumpStatsAsXml(true);
			}

		} catch (final Exception exception) {
			this.logger.error(exception.getMessage(), exception);
		}
		return "UNKNOW";
	}


	/**
	 * @return the jmxName
	 */
	public String getJmxName() {

		return this.jmxName;
	}


	/**
	 * @param jmxName
	 *            the jmxName to set
	 */
	public void setJmxName(final String jmxName) {

		this.jmxName = jmxName;
	}

}
