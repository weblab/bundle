/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.bundle.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;

import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.ow2.weblab.bundle.server.WebLabServer;
import org.ow2.weblab.bundle.utils.manager.ProcessManager;

/**
 * Utility class for processes
 *
 * @author asaval
 *
 */
public class ProcessUtils {


	private static final long PROCESS_MAX_WAITING_TIME = 60000L;


	private static final long PROCESS_STEP_WAITING_TIME = 200L;


	private ProcessUtils() {

	}


	/**
	 * Runs a process with a command in a dir in a given environment
	 *
	 * @param logger
	 *            the logger
	 * @param dir
	 *            the dir from which the process will be run
	 * @param env
	 *            the process environment
	 * @param commands2
	 *            commands to launch the process
	 * @return the process or null if it failed
	 */
	public static Process runProcess(final Log logger, final String dir, final Env env, final String... commands2) {

		String[] commands = commands2;
		if ((commands.length == 2) && "".equals(commands[1])) {
			commands = new String[] { commands[0] };
		}
		final ProcessBuilder pbuilder = new ProcessBuilder(commands);
		pbuilder.directory(new File(dir));
		if (env != null) {
			pbuilder.environment().putAll(env.details);
		}

		if (logger.isDebugEnabled()) {
			logger.debug("Running process " + Arrays.toString(commands));
			logger.debug("Running process in dir " + dir + " <=> " + new File(dir).getAbsolutePath());
			logger.debug("Running process in env " + pbuilder.environment());
		}

		for (final String arg : commands) {
			if (arg == null) {
				logger.error("Null argument in the command line : " + Arrays.asList(commands) + ". Aborting process.");
				return null;
			}
		}

		final Process process;
		try {
			process = pbuilder.start();
			new StreamGobbler(logger, process.getErrorStream(), false, Level.WARNING).start();
			new StreamGobbler(logger, process.getInputStream(), false, Level.FINE).start();
		} catch (final IOException e) {
			logger.error(e.getLocalizedMessage(), e);
			return null;
		}

		try {
			process.exitValue();
			return process;
		} catch (@SuppressWarnings("unused") final IllegalThreadStateException ignored) {
			// Ignored
		}

		long currentWaitingTime = 0L;

		while (currentWaitingTime < ProcessUtils.PROCESS_MAX_WAITING_TIME) {
			try {
				Thread.sleep(ProcessUtils.PROCESS_STEP_WAITING_TIME);
				currentWaitingTime += ProcessUtils.PROCESS_STEP_WAITING_TIME;
				process.exitValue();
				break;
			} catch (@SuppressWarnings("unused") final IllegalThreadStateException ignored) {
				if (logger.isDebugEnabled()) {
					logger.debug("Waiting end of command " + Arrays.toString(commands) + " " + currentWaitingTime + "ms");
				}
			} catch (@SuppressWarnings("unused") final InterruptedException ignored) {
				// Ignored
			}
		}

		return process;
	}


	/**
	 * Stop with a message it process failed.
	 *
	 * @param logger
	 *            The logger used inside
	 * @param process
	 *            the process
	 * @param message
	 *            the message
	 * @param code
	 *            The error code to return in case of failure
	 */
	public static void okOrFail(final Log logger, final Process process, final String message, final int code) {

		try {
			if (process == null) {
				logger.error(message);
				System.exit(code);
				return;
			}
			// add a timeout, if process is not returned, there might be a problem.
			logger.debug("Waiting 3s for the process to end.");
			synchronized (process) {
				process.wait(3000);
			}
			final int ret = process.exitValue();
			if (ret != 0) {
				logger.error(message);
				System.exit(code);
			}
		} catch (final IllegalThreadStateException exception) {
			logger.debug(exception.getMessage(), exception);
		} catch (final Exception exception) {
			logger.error(exception.getMessage(), exception);
		}
	}


	/**
	 * Return the output of the run process
	 *
	 * @param processBuilder
	 *            the process to run
	 * @param logger
	 *            a logger
	 * @return the output of the run process
	 * @throws IOException
	 *             If the process cannot be started (or an error occurred when calling start at least)
	 * @throws InterruptedException
	 *             If an error occurred when waiting for the end of the process
	 */
	public static List<String> execute(final ProcessBuilder processBuilder, final Log logger) throws IOException, InterruptedException {

		final ProcessBuilder processb = processBuilder;
		logger.debug("Executing commands: " + processBuilder.command() + ((processBuilder.directory() == null) ? "" : " in " + processBuilder.directory()));
		final Process process = processb.start();
		final StreamGobbler stream = new StreamGobbler(logger, process.getInputStream(), true, Level.OFF);
		stream.start();
		process.waitFor();

		return new LinkedList<>(stream.getData());
	}


	/**
	 * Try to run execute function during some time, repeating execution every second until timeout or success (return value not null
	 * if failed, print last error
	 *
	 * @param logger
	 *            The logger used inside
	 * @param executor
	 *            the Executor
	 * @param millis
	 *            time to wait
	 * @param <T>
	 *            The type returned by the executor
	 * @return result of execute function or null if it failed
	 */
	public static <T> T tryToExecuteDuring(final Log logger, final long millis, final Executor<T> executor) {

		T state = null;
		Exception last = null;
		final long time = System.currentTimeMillis();
		logger.debug("Trying to execute action during " + millis + "ms.");
		while ((state == null) && ((time + millis) > System.currentTimeMillis())) {
			executor.start();
			Thread.yield();
			try {
				Thread.sleep(1000);
			} catch (final InterruptedException e) {
				logger.error(e.getLocalizedMessage(), e);
			}
			state = executor.getStatus();
			last = executor.getLastException();
			if (last != null) {
				logger.debug(last.getLocalizedMessage(), last);
				// it might happen, but it is standard behavior since executed commands might fail
			}
		}

		if ((state == null) && (last != null)) {
			if (executor.isDisplayingFail()) {
				logger.error(last.getLocalizedMessage());
			}
			logger.debug(last.getLocalizedMessage(), last);
		}
		return state;
	}



	/**
	 * Ease environment construction for process
	 *
	 * @author asaval
	 */
	public static final class Env {


		final Map<String, String> details = new HashMap<>();


		private Env() {

		}


		/**
		 * @param details
		 *            Initial map
		 */
		public Env(final Map<String, String> details) {

			if (details != null) {
				for (final Entry<String, String> environmentEntry : details.entrySet()) {
					this.details.put(environmentEntry.getKey(), environmentEntry.getValue().replaceAll("\\s+", " ").trim());
				}
			}
		}


		/**
		 * Default empty environment.
		 *
		 * @return the default environment
		 */
		public static Env defaut() {

			return new Env();
		}


	}


	/**
	 *
	 * Class in charge of consuming an input stream and optionally to write it into the provided logger
	 *
	 * @author ymombrun
	 */
	public static class StreamGobbler extends Thread {


		InputStream istream;


		Log logger;


		boolean keepData = false;


		List<String> data = Collections.synchronizedList(new LinkedList<String>());


		int level = Level.FINE.intValue();


		boolean end = false;


		/**
		 * reads everything from is until empty.
		 *
		 * @param logger
		 *            The logger to write into
		 * @param istream
		 *            The input stream to read
		 * @param keepData
		 *            Whether or not to keep log entries
		 * @param level
		 *            The level of the logs to be written
		 */
		public StreamGobbler(final Log logger, final InputStream istream, final boolean keepData, final Level level) {

			this.istream = istream;
			this.logger = logger;
			this.level = level.intValue();
			this.keepData = keepData;
		}


		/**
		 * @return The messages read by this steam gobbler
		 */
		public List<String> getData() {

			while (!this.end) {
				Thread.yield();
				try {
					Thread.sleep(1000);
				} catch (final InterruptedException ioe) {
					this.logger.error(ioe.getLocalizedMessage(), ioe);
				}
			}
			return this.data;
		}


		@Override
		public void run() {

			try (InputStreamReader isr = new InputStreamReader(this.istream); final BufferedReader buffer = new BufferedReader(isr);) {

				String line = null;
				while ((line = buffer.readLine()) != null) {
					if (this.keepData) {
						this.data.add(line);
					}
					if (this.level < Level.FINER.intValue()) {
						// Ignore it
					} else if (this.level < Level.INFO.intValue()) {
						this.logger.debug(line);
					} else if (this.level < Level.WARNING.intValue()) {
						this.logger.info(line);
					} else if (this.level < Level.SEVERE.intValue()) {
						this.logger.warn(line);
					} else if (this.level < Level.OFF.intValue()) {
						this.logger.error(line);
					}
					Thread.yield();
				}
			} catch (final IOException ioe) {
				this.logger.error(ioe.getLocalizedMessage(), ioe);
			} finally {
				this.end = true;
			}
		}
	}


	/**
	 * Create a dummy prccess
	 *
	 * @return a dummy process definition
	 */
	public static Process dummyProccess() {

		return new Process() {


			@Override
			public int waitFor() throws InterruptedException {

				return 0;
			}


			@Override
			public OutputStream getOutputStream() {

				return null;
			}


			@Override
			public InputStream getInputStream() {

				return null;
			}


			@Override
			public InputStream getErrorStream() {

				return null;
			}


			@Override
			public int exitValue() {

				return 0;
			}


			@Override
			public void destroy() {

				// do nothing
			}
		};
	}


	/**
	 * This function will wait a bit for the server to shutdown, if it takes too long,
	 * it will start to send nice kill signal.
	 * If the server really to do end; then it asks the user if it should really kill it.
	 *
	 * @param logger
	 *            a logger
	 * @param server
	 *            a server
	 * @param force
	 *            if false, ask the user before killing the server, if true, we do not ask and kill it after 60s
	 */
	public static void dieAfterTimeout(final Log logger, final WebLabServer server, final boolean force) {

		final ProcessManager procManager = ProcessManager.getInstance();

		try {
			Thread.yield();
			logger.debug("Checking if " + server.getName() + " is ended");
			int pid = -1;
			final long time = System.currentTimeMillis() + (1000 * 30);
			boolean killed = false;
			boolean info = false;

			// we begin to have some concern and tell to process to die
			while (time > System.currentTimeMillis()) {
				Thread.yield();
				Thread.sleep(1000);
				// check if process if up every second
				pid = ProcessUtils.getProcessId(logger, server);

				if (pid == -1) {
					logger.debug("Ok, we remove the pid file " + pid);
					// process is dead, all is well that ends well
					// we remove pidfile if any
					ProcessUtils.checkPidFile(logger, server, true);
					return;
				} else if (!killed) {
					// we send a kill signal
					logger.debug("We send a stop signal to process " + pid);
					procManager.kill(logger, pid, false);
					killed = true;
				} else {
					// process is still here, show countdown before really killing the process
					if (!info) {
						logger.info(server.getName() + " can not stop, waiting " + ((time - System.currentTimeMillis()) / 1000) + "s before asking to kill it. Interrupt to abort.");
						info = true;
					}
					logger.debug("Seconds remaining before kill : " + ((time - System.currentTimeMillis()) / 1000) + "s");
				}
			}
			if (pid != -1) {
				String answer = "y";
				if (!force) {
					// we ask the user if he wants to terminate the process
					logger.info(server.getName() + " seems a bit slow to terminate, do you want to kill its process (" + pid + ") ? y/N");
					final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
					answer = br.readLine();
				}
				if ((answer != null) && "y".equalsIgnoreCase(answer)) {
					logger.debug("We really kill " + pid);
					// we just kill the process
					procManager.kill(logger, pid, true);
					// then we remove pidfile if any
					ProcessUtils.checkPidFile(logger, server, true);
				}
			}

		} catch (@SuppressWarnings("unused") final InterruptedException e) {
			// don't care
		} catch (final IOException e) {
			logger.error(e.getMessage(), e);
		}
	}


	/**
	 * Create a pid file from a WebLab server
	 *
	 * @param server
	 *            a weblab Server
	 * @return a file that contains the pid of the server
	 */
	private static File createPidFile(final WebLabServer server) {

		return new File(PathUtils.createPath(PathUtils.getWebLabHome("data"), "pids", server.getName() + ".pid"));
	}


	/**
	 * Create a pid file for the server
	 *
	 * @param logger
	 *            a logger
	 * @param server
	 *            a server
	 */
	public static void createPids(final Log logger, final WebLabServer server) {

		final ProcessManager procManager = ProcessManager.getInstance();
		try {
			final int pid = procManager.getPID(logger, server);
			final File pidFile = ProcessUtils.createPidFile(server);
			logger.debug("Creating pid file for " + server.getName() + " (" + pid + ") in " + pidFile.getAbsolutePath());
			FileUtils.writeStringToFile(pidFile, Integer.toString(pid));
		} catch (final Exception exception) {
			logger.error("Could not create pid file for Server : " + server.getName() + " " + exception.getMessage(), exception);
		}
	}


	/**
	 * Check if pid file exists and current state of server and return true if it matches current Server pid.
	 * It will remove the pid file if not process for the server is running
	 *
	 * @param logger
	 *            a logger
	 * @param server
	 *            a server
	 * @param cleanup
	 *            clear pid file once server is stopped
	 * @return true if pid from file match current process pid for this server
	 */
	public static boolean checkPidFile(final Log logger, final WebLabServer server, final boolean cleanup) {

		final ProcessManager procManager = ProcessManager.getInstance();
		final File pidFile = ProcessUtils.createPidFile(server);
		try {
			final int pid = procManager.getPID(logger, server);
			if (pidFile.exists()) {
				final String pidFromFile = FileUtils.readFileToString(pidFile);
				if (pid == -1) {
					final String msg1 = pidFile.getAbsolutePath() + " exists but " + server.getName() + " server is not running.";
					final String msg2 = "Deleting pid file for old process " + pidFromFile + ".";
					if (cleanup) {
						logger.warn(msg1);
						logger.warn(msg2);
					} else if (logger.isDebugEnabled()) {
						logger.debug(msg1);
						logger.debug(msg2);
					}
					if (!pidFile.delete()) {
						logger.error("Could not delete obsolete pid file: " + pidFile.getAbsolutePath());
					}
				} else if (!pidFromFile.equals(pid + "")) {
					logger.error("Pid " + pid + " for server " + server.getName() + " does not match pid (" + pidFromFile + ") from pid file " + pidFile.getAbsolutePath());
				} else {
					// pid == pidFrom File
					return true;
				}
			}
			return false;
		} catch (final IOException exception) {
			logger.error("Could not access pid file " + server.getName() + " for Server : " + server.getName() + " " + exception.getMessage(), exception);
		}
		return false;
	}


	/**
	 * Return the PID of the WebLabServer or -1 if the server is not started
	 *
	 * @param logger
	 *            a logger
	 * @param webLabServer
	 *            a WebLab Server
	 * @return the PID of the WebLabServer or -1 if the server is not started
	 */
	public static int getProcessId(final Log logger, final WebLabServer webLabServer) {

		final int pid = webLabServer.getPid();
		if (pid != -1) {
			return pid;
		}

		// else we search for it
		final ProcessManager procManager = ProcessManager.getInstance();
		return procManager.getPID(logger, webLabServer);
	}


	/**
	 * Set standard access permission on weblab home:
	 * read/write/excute acceses to the user, read/execute to everyone else to each subdirs and files
	 *
	 * @param logger
	 *            a logger
	 */
	public static void setStandardPermissions(final Log logger) {

		logger.debug("First Launched detected. Setting standard permissions.");
		final ProcessManager procManager = ProcessManager.getInstance();
		procManager.setPermission(PathUtils.getWebLabHome(""), "755", true, logger);
	}


	/**
	 * Creates a MBeanServerConnection to a remote or local host
	 *
	 * @param host
	 *            The host name
	 * @param port
	 *            The JMX port
	 * @param login
	 *            The login might be null if there is no authentication
	 * @param password
	 *            The password, must be null or empty if there is no authentication
	 * @param path
	 *            The jmxrmi path to the bean
	 * @param timeout
	 *            THe time to wait when opening the connection
	 * @param log
	 *            Whether or not to display connection error
	 * @param logger
	 *            The logger used inside
	 * @return a MBeanServerConnection
	 */
	public static JMXConnector createJMXclient(final String host, final int port, final String login, final String password, final String path, final int timeout, final boolean log,
			final Log logger) {

		return ProcessUtils.tryToExecuteDuring(logger, timeout, new Executor<JMXConnector>(log) {


			@Override
			public JMXConnector execute() throws Exception {

				final JMXServiceURL url = new JMXServiceURL("service:jmx:rmi:///jndi/rmi://" + host + ":" + port + "/" + path);
				final Hashtable<String, Object> env = new Hashtable<>();
				env.put("java.naming.factory.initial", "com.sun.jndi.rmi.registry.RegistryContextFactory");
				env.put("jmx.remote.x.client.connection.check.period", Long.valueOf(0));
				if ((password != null) && !password.equals("")) {
					env.put(JMXConnector.CREDENTIALS, new String[] { login, password });
				}
				return JMXConnectorFactory.connect(url, env);
			}
		});
	}


	/**
	 * Create a MbeanServerConnection from a WebLabServer
	 *
	 * @param server
	 *            The server, used to get the jmx port and the timeout
	 * @param log
	 *            Whether or not to display connection error
	 * @param logger
	 *            The logger used inside
	 * @return a MbeanServerConnection from a WebLabServer
	 */
	public static JMXConnector createJMXclient(final WebLabServer server, final boolean log, final Log logger) {

		return ProcessUtils.createJMXclient("localhost", server.getJmxPort(), "", "", "jmxrmi", server.getTimeout(), log, logger);
	}

}
