/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.bundle.server;

import javax.management.remote.JMXConnector;

public class DummyBus extends Bus {


	@Override
	public Process execute(String command) {
		return null;
	}


	@Override
	public String getProcessIdentificationClue() {
		return null;
	}


	@Override
	public Process start() {
		return null;
	}


	@Override
	public void stop() {
		// Nothing
	}


	@Override
	public boolean isServerFullyStarted(boolean showDetails) {
		return false;
	}


	@Override
	public JMXConnector getJMXConnector() {
		return null;
	}


	@Override
	public boolean startChain(String chain) {
		return false;
	}


	@Override
	public boolean stopChain(String chain) {
		return false;
	}


	@Override
	public void listChains() {
		// Nothing done
	}


	@Override
	public String statusChain(String chain) {
		return null;
	}


	@Override
	protected String[] getStopScript() {
		return null;
	}


	@Override
	protected String[] getStartScript() {
		return null;
	}

}
