/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.bundle.utils.manager;

import java.util.List;

import org.apache.commons.logging.Log;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Test;
import org.ow2.weblab.bundle.utils.Utils;

public class ProcessManagerTest {


	@Test
	public void testWindowsProcManager() {
		Assume.assumeTrue(System.getProperty("os.name").contains("win") || System.getProperty("os.name").contains("Win"));
		final String mockProcList = "\"java.exe\",\"1234\",\"Test\",\"0\",\"12Ko\"\n";
		WindowsProcessManager wpm = new WindowsProcessManager() {


			@Override
			protected ProcessBuilder getProcessCommand() {
				return new ProcessBuilder("cmd.exe", "/c", "echo", mockProcList);
			}

		};

		Log logger = Utils.getLogger();
		List<ProcessInfo> infos = wpm.list(logger);
		Assert.assertNotNull(infos);
		Assert.assertEquals(1, infos.size());
		ProcessInfo info = infos.get(0);
		Assert.assertEquals(1234, info.getPid());
		Assert.assertEquals("java.exe", info.getCommand());
	}


	@Test
	public void testLinuxProcManager() {
		Assume.assumeTrue(!(System.getProperty("os.name").contains("win") || System.getProperty("os.name").contains("Win") ));
		final String mockProcList = "1234 java test 123";
		LinuxProcessManager lpm = new LinuxProcessManager() {


			@Override
			protected ProcessBuilder getProcessCommand() {
				return new ProcessBuilder("echo", mockProcList);
			}
		};

		Log logger = Utils.getLogger();
		List<ProcessInfo> infos = lpm.list(logger);
		Assert.assertNotNull(infos);
		Assert.assertEquals(1, infos.size());
		ProcessInfo info = infos.get(0);
		Assert.assertEquals(1234, info.getPid());
		Assert.assertEquals("java test 123", info.getCommand());
	}

}