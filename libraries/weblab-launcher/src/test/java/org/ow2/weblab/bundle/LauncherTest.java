/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.bundle;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.bundle.conf.WebLabBean;
import org.ow2.weblab.bundle.utils.PathUtils;
import org.ow2.weblab.bundle.utils.Utils;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.core.io.FileSystemResource;

public class LauncherTest {


	/**
	 * Try to read the configuration.xml file provided in bundle/conf folder.
	 * 
	 * Rely on the configuration of properties plugin that generates a file maven-filtered.properties with the properties of the pom. This enable to resolve values like serverXXX.path that are
	 * replaced at packaging time.
	 */
	@Test
	public void testConfigLoading() {

		System.setProperty("weblab.home", PathUtils.getWebLabHome(""));
		System.setProperty("weblab.conf", PathUtils.getWebLabHome("conf"));
		System.setProperty("weblab.data", PathUtils.getWebLabHome("data"));

		final DefaultListableBeanFactory factory = new DefaultListableBeanFactory();
		final XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(factory);
		reader.loadBeanDefinitions(new FileSystemResource("../../conf/configuration.xml"));

		PropertyPlaceholderConfigurer cfg = new PropertyPlaceholderConfigurer();
		if (factory.containsBean("properties")) {
			cfg = factory.getBean("properties", PropertyPlaceholderConfigurer.class);
			cfg.setIgnoreUnresolvablePlaceholders(false);
			final FileSystemResource filterFile = new FileSystemResource("target/maven-filtered.properties");
			Assert.assertTrue("Filter file " + filterFile.getFile() + " does not exists.", filterFile.getFile().exists());
			cfg.setLocation(filterFile);
		}

		cfg.postProcessBeanFactory(factory);
		final WebLabBean wlb = factory.getBean("WebLab", WebLabBean.class);

		Assert.assertNotNull(wlb);
	}


	@Test
	public void testPaths() throws IOException {

		final String file = PathUtils.createPath("src", "test", "resources") + "cxf-servlet.xml";
		final String replacement = "<constructor-arg value=\"test\"/>";

		Map<String, String> toReplace = new HashMap<>();
		toReplace.put("<constructor-arg value=\"simple-repo\" />", replacement);
		Utils.update(Utils.getLogger(), toReplace, file);

		final String data = FileUtils.readFileToString(new File(file));

		Assert.assertTrue(data.contains(replacement));

		// reset
		toReplace = new HashMap<>();
		toReplace.put(replacement, "<constructor-arg value=\"simple-repo\" />");
		Utils.update(Utils.getLogger(), toReplace, file);
	}

}
